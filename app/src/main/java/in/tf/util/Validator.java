package in.tf.util;

import android.content.Context;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by kailash on 7/3/17.
 */

public class Validator {

    public static boolean isValidString(EditText value, Context context, String errorMsz) {
        boolean status = value != null && !value.getText().toString().equals("");
        if (!status) {
            AppMethods.showMessage(context, errorMsz);
        }
        return status;
    }

    public static boolean isValidURL(String url) {
        if (url == null) {
            return false;
        }
        // Assigning the url format regular expression
        String urlPattern = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
        return url.matches(urlPattern);
    }

    public static boolean isValidString(String value) {
        return value != null && !value.contentEquals("null") && !value.equals("");
    }

    public static boolean isValidPan(String pan) {
        Pattern mPattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");

        Matcher mMatcher = mPattern.matcher(pan);
        return mMatcher.matches();
    }

    public static boolean isValidInteger(Integer value) {
        return value != null;
    }

    public static int validInteger(Integer value) {
        if (value != null) {
            return value;
        } else {
            return 0;
        }
    }

    public static int validInteger(String value) {
        if (value != null) {
            int i;
            try {
                i = Integer.parseInt(value);

            } catch (Exception e) {
                i = 0;
            }
            return i;
        } else {
            return 0;
        }
    }

    public static String validString(String value) {

        if (value != null && !value.contentEquals("null") && !value.equals("")) {
            return value;
        } else {
            return "";
        }
    }


    public static long validLong(String timeStamp) {
        try {
            return Long.parseLong(timeStamp);
        } catch (Exception e) {
            return 0L;
        }
    }

    public static boolean isValidEmailAddress(CharSequence email) {
        if (email != null && !email.equals("")) {
            String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
            Pattern p = Pattern.compile(ePattern);
            Matcher m = p.matcher(email);
            return m.matches();
        } else {
            return false;
        }
    }

    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target.length() != 10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    public static boolean isFullNameValid(String str) {
        String expression = "^[a-zA-Z\\s]+";
        return str.matches(expression);
    }

}
