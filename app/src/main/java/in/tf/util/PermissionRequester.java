package in.tf.util;

import android.Manifest;
import android.app.Activity;
import android.os.Build;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.content.ContextCompat.checkSelfPermission;


/**
 * Created by kailash on 4/5/17.
 */

public class PermissionRequester {


    public static boolean requestRequiredPermissionForVideo(Activity activity, int PERMISSION_CODE) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {

            if (checkSelfPermission(activity, Manifest.permission_group.STORAGE) == PERMISSION_GRANTED
                    && checkSelfPermission(activity, Manifest.permission.CAMERA) == PERMISSION_GRANTED
                //&& checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) == PERMISSION_GRANTED
                    ) {
                return true;

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(new String[]{
                                    Manifest.permission_group.STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.RECORD_AUDIO,
                            },
                            PERMISSION_CODE);
                    return false;

                }
            }
        }
        return false;
    }

    public static boolean requestRequiredPermissionForImage(Activity activity, int PERMISSION_CODE) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {

            if (checkSelfPermission(activity, Manifest.permission_group.STORAGE) == PERMISSION_GRANTED
                    && checkSelfPermission(activity, Manifest.permission.CAMERA) == PERMISSION_GRANTED
                //&& checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) == PERMISSION_GRANTED
                    ) {
                return true;

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(new String[]{
                                    Manifest.permission_group.STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA,
                            },
                            PERMISSION_CODE);
                    return false;

                }
            }
        }
        return false;
    }

    public static boolean requestRequiredPermissionForSMS(Activity activity, int PERMISSION_CODE) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {

            if (checkSelfPermission(activity, Manifest.permission_group.SMS) == PERMISSION_GRANTED) {
                return true;

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(new String[]{
                                    Manifest.permission_group.STORAGE,
                                    Manifest.permission.READ_SMS,

                            },
                            PERMISSION_CODE);
                    return false;

                }
            }
        }
        return false;
    }

}

