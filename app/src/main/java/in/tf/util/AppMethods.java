package in.tf.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import in.tf.model.verification.ImageVerificationResponse;
import in.tf.network.APIService;
import in.tf.network.RequestResponses;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.tf.constants.Constants.selctedDocTypeId;

/**
 * Created by Mohan on 18-01-2016.
 */
public class AppMethods {

    public static boolean isConnected(Context context) {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet

            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }

    public static int getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return width;
    }

    public static void showMessage(Context context, String msg) {
        if (context != null && msg != null)
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showLog(String TAG, String message) {
        Log.e(TAG, message);
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static void hideKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void uploadImage(Context context, String userId, String filePath, String paramId) {

        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFile =
                MultipartBody.Part.createFormData(paramId, file.getName(), requestFile);
        RequestBody requestBodyDocType =
                RequestBody.create(MediaType.parse("multipart/form-data"), selctedDocTypeId+"");
        RequestBody requestBodyUserId =
                RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.saveImage(requestBodyUserId, requestBodyDocType, imageFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageVerificationResponse>() {
                    @Override
                    public void onCompleted() {
                        AppLog.e("=========== completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        AppLog.e("=========== fail");
                    }

                    @Override
                    public void onNext(ImageVerificationResponse response) {

                    }
                });
    }

    public static void uploadImageProfileImage(Context context, String userId, String filePath, String paramId) {

        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part imageFile =
                MultipartBody.Part.createFormData(paramId, file.getName(), requestFile);
        RequestBody requestBodyDocType =
                RequestBody.create(MediaType.parse("multipart/form-data"), "1");
        RequestBody requestBodyUserId =
                RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.saveProfileImage(requestBodyUserId, requestBodyDocType, imageFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageVerificationResponse>() {
                    @Override
                    public void onCompleted() {
                        AppLog.e("=========== completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        AppLog.e("=========== fail");
                    }

                    @Override
                    public void onNext(ImageVerificationResponse response) {

                    }
                });
    }

    public static void uploadVideo(Context context, String userId, String filePath, String paramId) {

        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("video/*"), file);
        MultipartBody.Part videoFile =
                MultipartBody.Part.createFormData(paramId, file.getName(), requestFile);
        RequestBody requestBodyDocType =
                RequestBody.create(MediaType.parse("multipart/form-data"), "1");
        RequestBody requestBodyUserId =
                RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.saveVideo(requestBodyUserId, requestBodyDocType, videoFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ImageVerificationResponse>() {
                    @Override
                    public void onCompleted() {
                        AppLog.e("=========== completed video");
                    }

                    @Override
                    public void onError(Throwable e) {
                        AppLog.e("=========== video fail");
                    }

                    @Override
                    public void onNext(ImageVerificationResponse response) {

                    }
                });
    }

    //video upload
    public static String multipartRequest(String urlTo, String requestType, Map<String, String> parmas, String filepath, String filefield, String fileMimeType) {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;

        String twoHyphens = "--";
        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String lineEnd = "\r\n";

        String result = "";

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        String[] q = filepath.split("/");
        int idx = q.length - 1;

        try {
            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);

            URL url = new URL(urlTo);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod(requestType);
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }

            outputStream.writeBytes(lineEnd);

            // Upload POST Data
            Iterator<String> keys = parmas.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = parmas.get(key);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(value);
                outputStream.writeBytes(lineEnd);
            }

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


            if (201 == connection.getResponseCode()) {
            } else if (200 != connection.getResponseCode()) {
                throw new Exception("Failed to upload code:" + connection.getResponseCode() + " " + connection.getResponseMessage());
            }

            inputStream = connection.getInputStream();

            result = AppMethods.convertStreamToString(inputStream);

            fileInputStream.close();
            inputStream.close();
            outputStream.flush();
            outputStream.close();

            return result;
        } catch (Exception e) {
            showLog("multipart upload error ", e.getMessage());
        }
        return null;

    }


    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    public static void deleteFiles(String... file) {
        for (int i = 0; i < file.length; i++) {
            File srcFile = new File(file[i]);
            if (srcFile.exists()) {
                boolean deleted = srcFile.delete();
                showLog("file delete status at ", i + " " + deleted);
            }
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static String convertDate(String inputDate) {
        String newDate = "";

        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");
            Date date = inputFormat.parse(inputDate);
            newDate = outputFormat.format(date);
        } catch (Exception e) {
        }

        return newDate;
    }

    public static void saveImage(Bitmap originalBitmap, File file) {

        try {
            FileOutputStream out = new FileOutputStream(file);

            // NEWLY ADDED CODE STARTS HERE [
            Canvas canvas = new Canvas(originalBitmap);

            Paint paint = new Paint();
            paint.setColor(Color.WHITE); // Text Color
            paint.setTextSize(12); // Text Size
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
            // some more settings...

            canvas.drawBitmap(originalBitmap, 0, 0, paint);
            canvas.drawText("" + System.currentTimeMillis(), 10, 10, paint);
            // NEWLY ADDED CODE ENDS HERE ]

            originalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
