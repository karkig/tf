package in.tf.util;

import android.content.Context;
import android.net.Uri;
import android.widget.MediaController;
import android.widget.VideoView;
public class VideoPlayer {

    public static void playVideo(VideoView videoView, Context context, String path) {
        //Creating MediaController
        MediaController mediaController = new MediaController(context);
        //specify the location of media file
        Uri uri = Uri.parse(path);
        //Setting MediaController and URI, then starting the videoView
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();
    }
}
