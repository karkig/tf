package in.tf.util;

import android.os.Environment;

import java.io.File;

import in.tf.constants.Constants;

/**
 * Created by kailash on 24/2/17.
 */

public class FileManager {
    public static final String VIDEO_FORMAT= ".mp4";
    public static final String IMAGE_FORMAT= ".jpeg";

    public static String getStoragePath() {
        File dir = new File(Environment.getExternalStorageDirectory(), "TF");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getAbsolutePath() + File.separator;
    }

    public static String getVerificationVideoPath() {
        return getStoragePath() + "verification_video_";
    }
    public static String getVerificationImagePath() {
        return getStoragePath() + "verification_image_";
    }


    public static void deleteFile(File file) {
        if (file != null && file.exists()) {
            boolean deleted = file.delete();
        }
    }
}
