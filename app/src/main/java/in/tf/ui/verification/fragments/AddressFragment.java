package in.tf.ui.verification.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.model.common.StateFilterResponse;
import in.tf.model.common.StateApiResponse;
import in.tf.model.personaldetails.address.CorrespondenceAddressRequest;
import in.tf.model.personaldetails.address.PermanentAddressRequest;
import in.tf.model.personaldetails.address.UserAddressRequest;
import in.tf.model.common.FilterRequest;
import in.tf.model.common.FilterResponse;
import in.tf.model.common.MasterApiCommonResponse;
import in.tf.model.registerlogin.CorrespondenceAddress;
import in.tf.model.registerlogin.PermanentAddress;
import in.tf.model.registerlogin.Person;
import in.tf.network.APIService;
import in.tf.network.CommonApi;
import in.tf.network.RequestResponses;
import in.tf.ui.dashboard.PersonalDetailsActivity_New;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import in.tf.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AddressFragment extends Fragment {
    @BindView(R.id.edtState)
    AutoCompleteTextView edtState;
    @BindView(R.id.edtCity)
    AutoCompleteTextView edtCity;
    @BindView(R.id.checkSame)
    CheckBox checkSame;
    @BindView(R.id.layoutPermanent)
    LinearLayout layoutPermanent;
    @BindView(R.id.edtAddress1)
    EditText edtAddress1;
    @BindView(R.id.edtAddress2)
    EditText edtAddress2;
    @BindView(R.id.edtCountry)
    EditText edtCountry;
    @BindView(R.id.edtPostal)
    EditText edtPostal;

    @BindView(R.id.edtPerAdd1)
    EditText edtPerAdd1;
    @BindView(R.id.edtPerLandMark)
    EditText edtPerLandMark;

    @BindView(R.id.edtPerState)
    AutoCompleteTextView edtPerState;
    @BindView(R.id.edtPerCity)
    AutoCompleteTextView edtPerCity;
    @BindView(R.id.edtPerPostal)
    EditText edtPerPostal;

    PersonalDetailsActivity_New context;
    List<StateFilterResponse> stateList;
    ArrayAdapter stateAdapter;
    String selectedStateCode;

    ArrayAdapter statePermanentAdapter;
    String selectedPermanentStateCode;

    List<FilterResponse> cityList;
    ArrayAdapter cityAdapter;
    Integer selectedCityCode;

    List<FilterResponse> cityPermanentList;
    ArrayAdapter cityPermanentAdapter;
    Integer selectedPermannetCityCode;
    ProgressDialog progressDrawable;
    int cAddressId = 0;
    int pAddressId = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_details,
                container, false);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (PersonalDetailsActivity_New) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        clicks();
        init();
        loadStateFromServer();
    }

    private void init() {
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
    }


    private void clicks() {
        checkSame.setOnCheckedChangeListener((compoundButton, b) -> layoutPermanent.setVisibility(b ? View.GONE : View.VISIBLE));

        edtState.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (StateFilterResponse filterResponse : stateList) {
                if (filterResponse.getValue().equals(itemName)) {
                    selectedStateCode = filterResponse.getKey();
                    break;
                }
            }
            loadCityForPresentAddress(selectedStateCode);
        });
        edtPerState.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (StateFilterResponse filterResponse : stateList) {
                if (filterResponse.getValue().equals(itemName)) {
                    selectedPermanentStateCode = filterResponse.getKey();
                    break;
                }
            }
            loadCityForPermanentAddress(selectedPermanentStateCode);
        });
        edtCity.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (FilterResponse filterResponse : cityList) {
                if (filterResponse.getValue().equals(itemName)) {
                    selectedCityCode = filterResponse.getKey();
                    break;
                }
            }
        });
        edtPerCity.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (FilterResponse filterResponse : cityPermanentList) {
                if (filterResponse.getValue().equals(itemName)) {
                    selectedPermannetCityCode = filterResponse.getKey();
                    break;
                }
            }
        });
    }

    private UserAddressRequest createRequest() {
        UserAddressRequest request = new UserAddressRequest();
        CorrespondenceAddressRequest correspondenceAddress = new CorrespondenceAddressRequest();

        correspondenceAddress.setAddressID(cAddressId);
        correspondenceAddress.setAddressType(null);

        if (Validator.isValidString(edtAddress1, context, "Enter valid Address ")) {
            correspondenceAddress.setAddressLine1(edtAddress1.getText().toString());
        } else {
            return null;
        }
        if (Validator.isValidString(edtAddress2, context, "Enter valid landmark")) {
            correspondenceAddress.setAddressRemark(edtAddress2.getText().toString());
        } else {
            return null;
        }

        if (Validator.isValidString(edtState, context, "Enter valid state")) {
            correspondenceAddress.setState(edtState.getText().toString());

        } else {
            return null;
        }
        if (Validator.isValidString(edtCity, context, "Enter valid city")) {
            correspondenceAddress.setCity(edtCity.getText().toString());
        } else {
            return null;
        }
        if (Validator.isValidString(edtPostal, context, "Enter valid postal code")) {
            correspondenceAddress.setPostCode(edtPostal.getText().toString());
        } else {
            return null;
        }
        correspondenceAddress.setCityID(selectedCityCode + "");
        correspondenceAddress.setStateID(selectedStateCode);
        correspondenceAddress.setCountryID("IN");
        correspondenceAddress.setAddressType(2);


        PermanentAddressRequest permanentAddress = new PermanentAddressRequest();

        if (checkSame.isChecked()) {
            permanentAddress.setAddressID(correspondenceAddress.getAddressID());
            permanentAddress.setAddressType(correspondenceAddress.getAddressType());
            permanentAddress.setAddressLine1(correspondenceAddress.getAddressLine1());
            permanentAddress.setAddressRemark(correspondenceAddress.getAddressRemark());
            permanentAddress.setCity(correspondenceAddress.getCity());
            permanentAddress.setCityID(correspondenceAddress.getCityID());
            permanentAddress.setState(correspondenceAddress.getState());
            permanentAddress.setStateID(correspondenceAddress.getStateID());
            permanentAddress.setPostCode(correspondenceAddress.getPostCode());
            permanentAddress.setCountryID("IN");
        } else {
            permanentAddress.setAddressID(pAddressId);
            permanentAddress.setAddressType(1);
            if (Validator.isValidString(edtPerAdd1, context, "Enter valid Address ")) {
                permanentAddress.setAddressLine1(edtPerAdd1.getText().toString());
            } else {
                return null;
            }
            if (Validator.isValidString(edtPerLandMark, context, "Enter valid landmark ")) {
                permanentAddress.setAddressRemark(edtPerLandMark.getText().toString());
            } else {
                return null;
            }

            if (Validator.isValidString(edtPerState, context, "Enter valid state ")) {
                permanentAddress.setState(edtPerState.getText().toString());
            } else {
                return null;
            }
            if (Validator.isValidString(edtPerCity, context, "Enter valid city ")) {
                permanentAddress.setCity(edtPerCity.getText().toString());
            } else {
                return null;
            }

            if (Validator.isValidString(edtPerPostal, context, "Enter valid postal code ")) {
                permanentAddress.setPostCode(edtPerPostal.getText().toString());
            } else {
                return null;
            }
            permanentAddress.setCityID(selectedPermannetCityCode + "");
            permanentAddress.setStateID(selectedPermanentStateCode);
            permanentAddress.setCountryID("IN");
        }
        request.setCorrespondenceAddress(correspondenceAddress);
        request.setPermanentAddress(permanentAddress);
        request.setPersonID(Pref.readInteger(context, Pref.PREF_USER_ID));

        return request;
    }

    public void updateAddress() {
        AppMethods.hideKeyboard(context);
        UserAddressRequest request = createRequest();
        if (request == null) {
            return;
        }
        progressDrawable.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.updateAddress(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<Person>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- AddressFragment - updateAddress" + throwable.getMessage());
                        progressDrawable.dismiss();

                    }

                    @Override
                    public void onNext(Response<Person> objectResponse) {
                        progressDrawable.dismiss();
                        PersonalDetailsActivity_New.person = objectResponse.body();
                        AppMethods.showMessage(context, "Address updated successfully.");

                    }
                });

    }


    private void loadCityForPresentAddress(String stateCode) {
        progressDrawable.show();

        CommonApi.getCityList(context, new FilterRequest(stateCode), data -> {
            cityList = ((MasterApiCommonResponse) data).list;
            ArrayList<String> filterList = new ArrayList<>();
            for (FilterResponse item : cityList) {
                filterList.add(item.getValue());
            }
            cityAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            edtCity.setAdapter(cityAdapter);
            progressDrawable.dismiss();

        });
    }

    private void loadCityForPermanentAddress(String stateCode) {
        progressDrawable.show();

        CommonApi.getCityList(context, new FilterRequest(stateCode), data -> {
            cityPermanentList = ((MasterApiCommonResponse) data).list;
            ArrayList<String> filterList = new ArrayList<>();
            for (FilterResponse item : cityPermanentList) {
                filterList.add(item.getValue());
            }
            cityPermanentAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            edtPerCity.setAdapter(cityPermanentAdapter);
            progressDrawable.dismiss();

        });
    }

    private void loadStateFromServer() {
        progressDrawable.show();
        CommonApi.getStateList(context, new FilterRequest("IN"), data -> {
            stateList = ((StateApiResponse) data).list;
            ArrayList<String> filterList = new ArrayList<>();
            for (StateFilterResponse item : stateList) {
                filterList.add(item.getValue());
            }
            stateAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            statePermanentAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            edtState.setAdapter(stateAdapter);
            edtPerState.setAdapter(statePermanentAdapter);
            progressDrawable.dismiss();
        });
    }

    public void updateViewWithData(Person person) {
        try {
            if (person == null) {
                return;
            }
            CorrespondenceAddress correspondAddress = person.getCorrespondenceAddress();
            cAddressId = correspondAddress.getAddressID();
            edtAddress1.setText(correspondAddress.getAddressLine1() + " " + correspondAddress.getAddressLine2());
            edtAddress2.setText(correspondAddress.getAddressRemark());
            edtCity.setText(correspondAddress.getCity());
            edtState.setText(correspondAddress.getState());
            edtPostal.setText(correspondAddress.getPostCode() + "");

            PermanentAddress permanentAddress = person.getPermanentAddress();
            pAddressId = permanentAddress.getAddressID();
            edtPerAdd1.setText(permanentAddress.getAddressLine1() + " " + permanentAddress.getAddressLine2());
            edtPerLandMark.setText(permanentAddress.getAddressRemark());
            edtPerCity.setText(permanentAddress.getCity());
            edtPerState.setText(permanentAddress.getState());
            edtPerPostal.setText(permanentAddress.getPostCode() + "");
        } catch (Exception e) {
            AppLog.e("===========> " + e.getMessage());
        }

    }
}