package in.tf.ui.verification.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.constants.IntentConstants;
import in.tf.dialogs.DocumentSelectDialog;
import in.tf.listeners.OnDocumentTypeSelected;
import in.tf.ui.dashboard.PersonalDetailsActivity_New;
import in.tf.ui.verification.activities.DetailViewOfDocumentActivity;
import in.tf.ui.verification.activities.VerifyImageActivity;
import in.tf.ui.verification.activities.VerifyVideoActivity;
import in.tf.util.AppMethods;
import in.tf.util.Pref;

public class DocumentVerifyFragment extends Fragment {

    @BindView(R.id.imageView)
    RoundedImageView imageView;
    @BindView(R.id.videoView)
    RoundedImageView videoView;
    @BindView(R.id.videoDetailView)
    LinearLayout videoDetailView;
    @BindView(R.id.imgDetailView)
    LinearLayout imgDetailView;
    @BindView(R.id.takePicture)
    Button takePicture;
    @BindView(R.id.videoContainer)
    RelativeLayout videoContainer;
    @BindView(R.id.imageContainer)
    RelativeLayout imageContainer;
    @BindView(R.id.takeVideo)
    Button takeVideo;
    @BindView(R.id.videoRetake)
    FloatingActionButton videoRetake;
    @BindView(R.id.imageRetake)
    FloatingActionButton imageRetake;
    PersonalDetailsActivity_New context;
    static int VIDEO_VERIFY_REQUEST_CODE = 100;
    static int IMAGE_VERIFY_REQUEST_CODE = 200;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (PersonalDetailsActivity_New) context;
    }

    private void clicks() {
        takePicture.setOnClickListener(view -> {

            DocumentSelectDialog dialog = new DocumentSelectDialog(context, tag -> {
                Intent intent = new Intent(context, VerifyImageActivity.class);
                intent.putExtra(IntentConstants.INTENT_DOC_TYPE, tag);
                context.startActivityForResult(intent, IMAGE_VERIFY_REQUEST_CODE);
            });
            dialog.show();

        });
        takeVideo.setOnClickListener(view -> {
            Intent intent = new Intent(context, VerifyVideoActivity.class);
            context.startActivityForResult(intent, VIDEO_VERIFY_REQUEST_CODE);
        });
        videoRetake.setOnClickListener(view -> {
            Intent intent = new Intent(context, VerifyVideoActivity.class);
            context.startActivityForResult(intent, VIDEO_VERIFY_REQUEST_CODE);
        });
        imageRetake.setOnClickListener(view -> {
            Intent intent = new Intent(context, VerifyImageActivity.class);
            context.startActivityForResult(intent, IMAGE_VERIFY_REQUEST_CODE);
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        clicks();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verification_doc,
                container, false);
        return view;
    }

    public void updateImageView(String imagePath) {
        File path = new File(imagePath);
        Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()), 640, 480);
        AppMethods.saveImage(bitmap, path);
        imageView.setImageBitmap(bitmap);
        imageContainer.setVisibility(View.VISIBLE);
        imgDetailView.setVisibility(View.GONE);
        AppMethods.uploadImage(context, Pref.readInteger(context, Pref.PREF_USER_ID)+"", imagePath, "file");
        imageView.setOnClickListener(view -> {
            Intent intent = new Intent(context, DetailViewOfDocumentActivity.class);
            intent.putExtra(IntentConstants.INTENT_IMAGE_PATH, imagePath);
            startActivity(intent);
        });
    }

    public void updateVideoView(String videoPath) {
        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(new File(videoPath).getAbsolutePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
        videoView.setImageBitmap(bMap);
        videoContainer.setVisibility(View.VISIBLE);
        videoDetailView.setVisibility(View.GONE);
        AppMethods.uploadVideo(context, Pref.readInteger(context, Pref.PREF_USER_ID)+"", videoPath, "file");
        videoView.setOnClickListener(view -> {
            Intent intent = new Intent(context, DetailViewOfDocumentActivity.class);
            intent.putExtra(IntentConstants.INTENT_VIDEO_PATH, videoPath);
            startActivity(intent);
        });
    }
}
