package in.tf.ui.verification.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.adapters.DocSpinnerAdapter;
import in.tf.components.CameraFragment;
import in.tf.constants.IntentConstants;
import in.tf.model.common.FilterRequest;
import in.tf.model.common.FilterResponse;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.network.APIService;
import in.tf.network.CommonApi;
import in.tf.network.CommonApiCallback;
import in.tf.network.RequestResponses;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.PermissionRequester;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.tf.constants.Constants.SUCCESS;
import static in.tf.constants.Constants.selctedDocTypeId;


/**
 * A login screen that offers login via email/password.
 */
public class VerifyImageActivity extends AppCompatActivity {


    @BindView(R.id.imageView)
    FrameLayout imageView;
    @BindView(R.id.spnrDocType)
    AppCompatSpinner spnrDocType;
    DocSpinnerAdapter spinnerArrayAdapter;
    ArrayList<String> spinnerArray = new ArrayList<>();
    VerifyImageActivity context;
    CameraFragment cameraFragment;
    List<FilterResponse> responses = new ArrayList<>();
    private static final int CAMERA_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_image);
        context = this;
        ButterKnife.bind(context);
        selctedDocTypeId = 0; // initializing to default value.
        setupSpinner();
        String selectedDocType = getIntent().getStringExtra(IntentConstants.INTENT_DOC_TYPE);
        CommonApi.getDocumentTypes(context, new FilterRequest(selectedDocType), data -> {
            responses.addAll((List<FilterResponse>) data);
            spinnerArrayAdapter.notifyDataSetChanged();
        });
        clicks();
        requestForPermission();
    }

    private void setupSpinner() {
        spinnerArrayAdapter = new DocSpinnerAdapter(context, responses);

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spnrDocType.setAdapter(spinnerArrayAdapter);
        spnrDocType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selctedDocTypeId = responses.get(i).getKey();
                AppLog.e("========selctedDocTypeId= " + selctedDocTypeId + " === " + responses.get(i).getValue());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    private void requestForPermission() {
        if (PermissionRequester.requestRequiredPermissionForImage(context, CAMERA_REQUEST_CODE)) {
            startCameraFragment();
        } else {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                //   && ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                    ) {
                startCameraFragment();
            } else {
                // checkRequestIsDeniedOrNot();
            }

        } else {
            // checkRequestIsDeniedOrNot();
        }
    }

    private void clicks() {

    }

    private void startCameraFragment() {
        cameraFragment = CameraFragment.newInstance();
        loadFragment(cameraFragment);
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
        fragmentManager.replace(R.id.imageView, fragment).commit();
    }


}


