package in.tf.ui.verification.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.VideoView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.application.TFApplication;
import in.tf.constants.IntentConstants;
import in.tf.util.AppLog;
import in.tf.util.VideoPlayer;


/**
 * A login screen that offers login via email/password.
 */
public class DetailViewOfDocumentActivity extends AppCompatActivity {
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.videoView)
    VideoView videoView;
    DetailViewOfDocumentActivity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_view_verification);
        context = this;
        ButterKnife.bind(context);
        String imagePath = getIntent().getStringExtra(IntentConstants.INTENT_IMAGE_PATH);
        String videoPath = getIntent().getStringExtra(IntentConstants.INTENT_VIDEO_PATH);
        if (null != imagePath && !imagePath.equals("")) {
            AppLog.e("-------- " + imagePath);
            ImageLoader.getInstance().displayImage(Uri.decode(Uri.fromFile(new File(imagePath)).toString()), image, TFApplication.noCacheImagLoader); // Default options will be used
        } else if (null != videoPath && !videoPath.equals("")) {
            VideoPlayer.playVideo(videoView, context, videoPath);
        }
    }
}


