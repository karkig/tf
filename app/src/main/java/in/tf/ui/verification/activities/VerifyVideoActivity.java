package in.tf.ui.verification.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.FileManager;
import in.tf.util.PermissionRequester;
import in.tf.views.CameraPreview;

/**
 * A login screen that offers login via email/password.
 */
public class VerifyVideoActivity extends AppCompatActivity {

    @BindView(R.id.videoview)
    FrameLayout videoview;
    @BindView(R.id.buttonlayout)
    LinearLayout buttonlayout;
    @BindView(R.id.imgVideoBtn)
    ImageView imgVideoBtn;
    VerifyVideoActivity context;
    private CameraPreview mPreview;
    private Camera videoCamera;
    private MediaRecorder mRecorder;
    private boolean isRecording = false;
    private boolean isfrontcamera = false;
    private int mPicturewidth, mpictureHeight, videoheight;
    private static final int PERMISSION_REQUEST_CODE = 100;
    private static final int CAMERA_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_video);
        context = this;
        ButterKnife.bind(context);
        clicks();
        requestForPermission();
    }

    private void clicks() {

        imgVideoBtn.setOnClickListener(view -> {
            String tag = imgVideoBtn.getTag().toString();
            if ("RECORDING".equalsIgnoreCase(tag)) {
                imgVideoBtn.setTag("UN_RECORDING");
                stopRecording();
            } else {
                imgVideoBtn.setTag("RECORDING");
                startRecording();
            }
        });
    }

    private void requestForPermission() {
        if (PermissionRequester.requestRequiredPermissionForVideo(context, PERMISSION_REQUEST_CODE)) {
        } else {
        }
    }

    public void initUi() {
        mPreview = new CameraPreview(context, videoCamera);
        mPreview.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        videoview.addView(mPreview);
    }

    @Override
    public void onResume() {
        super.onResume();
        WindowManager.LayoutParams params = this.getWindow().getAttributes();

/** Turn on screen light */
        params.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.screenBrightness = 0.9f;
        this.getWindow().setAttributes(params);
        if (!hasCamera(context)) {
            AppMethods.showMessage(context, "This device doesnt support this feature");
            finish();
        }
        if (videoCamera == null) {
            if (findCameraId(Camera.CameraInfo.CAMERA_FACING_FRONT) == 1) {
                isfrontcamera = false;
                releaseCamera();
                chooseCamera();
            } else {
                AppMethods.showMessage(context, "sorry the device supports only one camera");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                //   && ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                    ) {
            } else {
                checkRequestIsDeniedOrNot();
            }

        } else {
            checkRequestIsDeniedOrNot();
        }
    }

    private void checkRequestIsDeniedOrNot() {
    }

    private int findCameraId(int cameraFacing) {
        int cameraId = -1;
        int totalCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < totalCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == cameraFacing) {
                cameraId = i;
            }
            isfrontcamera = cameraFacing == Camera.CameraInfo.CAMERA_FACING_FRONT;
        }
        return cameraId;
    }


    private void chooseCamera() {
        try {
            int cameraId = 0;
            if (!isfrontcamera) {
                cameraId = findCameraId(Camera.CameraInfo.CAMERA_FACING_FRONT);
            } else {
                cameraId = findCameraId(Camera.CameraInfo.CAMERA_FACING_BACK);
            }
            if (cameraId >= 0) {
                videoCamera = Camera.open(cameraId);
                setCameraDefaults();
                initUi();
                mPreview.refreshVideoCamera(videoCamera);
                AppLog.e("------- " + "initialized ");

            }
        } catch (Exception e) {
            AppLog.e("------- " + e.getMessage());
            AppMethods.showMessage(context, "Unable to initialize camera or other app using it. ");
        }
    }

    private void setCameraDefaults() {
//        videoCamera.setDisplayOrientation(90);

        Camera.Parameters params = videoCamera.getParameters();
        if (params.getSupportedFocusModes() != null && params.getSupportedFocusModes() != null && params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

        }

        // Supported picture formats (all devices should support JPEG).
        List<Integer> formats = params.getSupportedPictureFormats();

        if (formats.contains(ImageFormat.JPEG)) {
            params.setPictureFormat(ImageFormat.JPEG);
            params.setJpegThumbnailQuality(90);
            params.setJpegQuality(90);
        } else
            params.setPictureFormat(PixelFormat.RGB_565);
        // Set the brightness to auto.
        params.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
        // For Android 2.2 and above
        videoCamera.setDisplayOrientation(90);
        Camera.Size picturesize = getOptimalPreviewSize(params.getSupportedPictureSizes(), AppMethods.getScreenWidth(context), videoheight);
        mPicturewidth = picturesize.width;
        mpictureHeight = picturesize.height;
//        mPreview.setLayoutParams(new FrameLayout.LayoutParams(picturesize.width,picturesize.height));
//        videolayout.setLayoutParams(new RelativeLayout.LayoutParams(picturesize.width,picturesize.height));
        Log.e("video picture size", " width: " + picturesize.width + " height: " + picturesize.height);
        videoCamera.setParameters(params);
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;
        if (sizes == null) return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public void releaseCamera() {
        //stop and release camera
        if (videoCamera != null) {
            videoCamera.release();
            videoCamera = null;
        }
    }

    private boolean hasCamera(Context ctx) {
        //check if device has camera
        return ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void startRecording() {

        new MediaPrepareTask().execute();
    }

    public boolean prepareMediaRecorder() {
        CamcorderProfile profile;
        profile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
        try {
            AppLog.e("---------- preparing media recorder");
            mRecorder = new MediaRecorder();
            videoCamera.unlock();
            mRecorder.setCamera(videoCamera);
            mRecorder.setOrientationHint(270);
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            //mRecorder.setVideoFrameRate(15);
            mRecorder.setProfile(profile);
            mRecorder.setOutputFile(FileManager.getVerificationVideoPath() + FileManager.VIDEO_FORMAT);
            mRecorder.prepare();
            AppLog.e("---------- media recorder prepared");

        } catch (IllegalStateException e) {
            AppLog.e(" in method prepareMediaRecorder ---------- " + e.getMessage());
            return false;
        } catch (Exception e) {
            AppLog.e("in method prepareMediaRecorder ---------- " + e.getMessage());

            return false;
        }
        return true;
    }

    public void releaseMediaRecorder() {
        if (mRecorder != null) {
            mRecorder.reset();
            mRecorder.release();
            mRecorder = null;
            if (videoCamera != null) {
                AppLog.e("--------- camera lock");
                videoCamera.lock();
            }
            AppLog.e("---------- released media recorder");

        }
    }

    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            // initialize video camera
            return startVideoSetup();
        }

        boolean startVideoSetup() {
            if (prepareMediaRecorder()) {
                // Camera is available and unlocked, MediaRecorder is prepared,
                try {
                    AppLog.e("---------- recorder started");
                    mRecorder.start();
                } catch (Exception e) {
                    AppLog.e("------ " + e.getMessage());
                }
                isRecording = true;
            } else {
                // prepare didn't work, release the camera
                releaseMediaRecorder();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            try {
                if (!result) {
                    finish();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 1000);

            } catch (Exception ex) {

            }
        }
    }

    public void stopRecording() {
        try {

            if (null != mRecorder) {
                mRecorder.stop();
                AppLog.e("----------  media recorder stopped");
            }
            releaseMediaRecorder();
            isRecording = false;
            AppLog.e("----------  video saved successfully");

            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();

        } catch (Exception e) {
            Log.d("stop recording", "Got exception " + e);
            AppMethods.showMessage(context, "Video not saved, Please try again.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        videoCamera.stopPreview();
        videoCamera.release();
        videoCamera = null;
        isRecording = false;
        mRecorder = null;
        mPreview = null;
    }
}

