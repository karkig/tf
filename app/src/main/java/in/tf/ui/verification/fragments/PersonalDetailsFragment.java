package in.tf.ui.verification.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.application.TFApplication;
import in.tf.model.personaldetails.PersonalDetailsDao;
import in.tf.model.registerlogin.Person;
import in.tf.network.APIService;
import in.tf.network.RequestResponses;
import in.tf.ui.dashboard.PersonalDetailsActivity_New;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import in.tf.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.tf.ui.dashboard.PersonalDetailsActivity_New.GALLERY_PICTURE;

public class PersonalDetailsFragment extends Fragment {

    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.profilePic)
    ImageView profilePic;
    @BindView(R.id.editPic)
    ImageView editPic;
    @BindView(R.id.edtBirthdy)
    TextView edtBirthdy;
    @BindView(R.id.edtAadhaarNo)
    EditText edtAadhaarNo;
    @BindView(R.id.edtPanNo)
    EditText edtPanNo;
    @BindView(R.id.radioSexGroup)
    RadioGroup radioSexGroup;
    @BindView(R.id.radioMale)
    RadioButton radioMale;
    PersonalDetailsActivity_New context;
    ProgressDialog progressDrawable;
    String DOB;
    private static final Calendar myCalendar = Calendar.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_details,
                container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        clicks();
        init();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (PersonalDetailsActivity_New) context;
    }

    private void init() {
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
    }

    private void clicks() {
        editPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPicSelectorDialog();
            }
        });
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {

            DOB = dayOfMonth + "/" + monthOfYear + "/" + year;
            edtBirthdy.setText(DOB);
        };

        edtBirthdy.setOnClickListener(v -> {
            new DatePickerDialog(context, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });
    }

    private void startPicSelectorDialog() {

        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        context.startActivityForResult(intent, GALLERY_PICTURE);
    }

    private PersonalDetailsDao applyValidation(Integer userId) {
        PersonalDetailsDao request = new PersonalDetailsDao();
        request.setPersonid(userId);
        if (Validator.isFullNameValid(edtName.getText().toString())) {
            request.setFullname(edtName.getText().toString());
        } else {
            AppMethods.showMessage(context, "Enter valid name");
            return null;
        }
        if (Validator.isValidEmailAddress(edtEmail.getText().toString())) {
            request.setEmail(edtEmail.getText().toString());
        } else {
            AppMethods.showMessage(context, "Enter valid email.");
            return null;
        }
        if (DOB != null) {
            request.setDateOfBirth(DOB);
        } else {
            AppMethods.showMessage(context, "Enter date of birth.");
            return null;
        }

        if (edtAadhaarNo.getText().length() == 12) {
            request.setAadharNumber(edtAadhaarNo.getText().toString());
        } else {
            AppMethods.showMessage(context, "Enter valid Aadhaar number.");
            return null;
        }
        if (Validator.isValidPan(edtPanNo.getText().toString())) {
            request.setPanNumber(edtPanNo.getText().toString());
        } else {
            AppMethods.showMessage(context, "Enter valid PAN.");
            return null;
        }
        if (Validator.isValidPhoneNumber(edtPhone.getText().toString())) {
            request.setPhone(edtPhone.getText().toString());
        } else {
            AppMethods.showMessage(context, "Enter valid phone number.");
            return null;
        }
        if (radioMale.isChecked()) {
            request.setGender("M");
        } else {
            request.setGender("F");
        }
        return request;
    }

    public void updatePersonalDetails() {
        PersonalDetailsDao param = applyValidation(Pref.readInteger(context, Pref.PREF_USER_ID));
        if (param == null) {
            return;
        }
        progressDrawable.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.updatePersonalDetails(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<Person>>() {
                    @Override
                    public void onCompleted() {
                        progressDrawable.dismiss();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                        progressDrawable.dismiss();
                    }

                    @Override
                    public void onNext(Response<Person> objectResponse) {
                        PersonalDetailsActivity_New.person = objectResponse.body();
                        AppMethods.showMessage(context, "Details saved successfully.");
                    }
                });
    }

    public void updateViewWithData(Person person) {
        if (person == null) {
            return;
        }
        edtName.setText(person.getFullName());
        edtEmail.setText(person.getEmail());
        edtAadhaarNo.setText(person.getAadharNumber());
        edtPanNo.setText(person.getPanNumber());
        edtPhone.setText(person.getPhone());
        edtBirthdy.setText(person.getDateOfBirth());
        DOB = person.getDateOfBirth();
        ImageLoader.getInstance().displayImage(person.getImageURL(), profilePic, TFApplication.cacheImagLoader); // Default options will be used

    }

    public void updateProfilePic(String url) {
        ImageLoader.getInstance().displayImage(Uri.decode(Uri.fromFile(new File(url)).toString()), profilePic, TFApplication.cacheImagLoader); // Default options will be used

    }
}