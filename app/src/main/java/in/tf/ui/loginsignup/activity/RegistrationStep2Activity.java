package in.tf.ui.loginsignup.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;

import com.chaos.view.PinView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.constants.IntentConstants;
import in.tf.model.registerlogin.Person;
import in.tf.model.registerlogin.RegisterRequest;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.network.APIService;
import in.tf.network.RequestResponses;
import in.tf.ui.dashboard.DashboardActivity;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import in.tf.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.tf.constants.Constants.SUCCESS;
import static in.tf.constants.IntentConstants.INTENT_USER_ID;


/**
 * A login screen that offers login via email/password.
 */
public class RegistrationStep2Activity extends AppCompatActivity {


    @BindView(R.id.completeBtn)
    Button completeBtn;
    @BindView(R.id.fullNameEdt)
    AutoCompleteTextView fullNameEdt;
    @BindView(R.id.emailEdt)
    AutoCompleteTextView emailEdt;
    @BindView(R.id.pinView)
    PinView pinView;
    @BindView(R.id.checkStudentAcnt)
    CheckBox checkStudentAcnt;
    @BindView(R.id.checkTermsCondition)
    CheckBox checkTermsCondition;
    RegistrationStep2Activity context;
    String userId;
    ProgressDialog progressDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        userId = getIntent().getStringExtra(IntentConstants.INTENT_USER_ID);
        context = this;
        ButterKnife.bind(context);
        clicks();
        init();
    }

    private void init() {
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
        pinView.setCursorVisible(true);
        pinView.setAnimationEnable(true);
        pinView.setCursorColor(getColor(R.color.colorAccent));
        pinView.setHideLineWhenFilled(false);

    }


    private void clicks() {
        completeBtn.setOnClickListener(v -> {
            AppMethods.hideKeyboard(context);
            register();
        });
    }

    private RegisterRequest prepareData() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUserid(userId);
        Person person = new Person();
        if (fullNameEdt.getText().toString() != null && !fullNameEdt.getText().toString().trim().equals("")) {
            person.setFullName(fullNameEdt.getText().toString());
        } else {
            AppMethods.showMessage(context, "Please enter a valid name");
            return null;
        }
        if (Validator.isValidEmailAddress(emailEdt.getText())) {
            person.setEmail(emailEdt.getText().toString());
        } else {
            AppMethods.showMessage(context, "Please enter a valid email id");
            return null;
        }
        if (!pinView.getText().toString().equals("")) {
            registerRequest.setpIN(pinView.getText().toString());
        } else {
            AppMethods.showMessage(context, "Please enter a PIN");
            return null;
        }

        if (!checkStudentAcnt.isChecked() || !checkTermsCondition.isChecked()) {
            AppMethods.showMessage(context, "Please select agree Terms and Conditions");
            return null;
        }
        registerRequest.setPerson(person);
        return registerRequest;
    }

    public void register() {
        RegisterRequest params = prepareData();
        if (params == null) {
            return;
        }
        progressDrawable.show();

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.registerUser(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<RegisterStep1Response>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        progressDrawable.dismiss();
                        AppMethods.showMessage(context, "Registration Failed");
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<RegisterStep1Response> objectResponse) {
                        progressDrawable.dismiss();
                        RegisterStep1Response response = objectResponse.body();
                        if (response.getStatus().equals(SUCCESS)) {
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }
                });
    }
}