package in.tf.ui.loginsignup.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.constants.IntentConstants;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.network.APIService;
import in.tf.network.RequestResponses;
import in.tf.ui.dashboard.DashboardActivity;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import in.tf.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.tf.constants.Constants.SUCCESS;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.nextBtn)
    Button nextBtn;
    @BindView(R.id.signup)
    TextView signup;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.pin)
    PinView pinView;
    LoginActivity context;
    ProgressDialog progressDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        ButterKnife.bind(context);
        clicks();
        init();
    }

    private void init() {
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
        phone.setText(Pref.readString(context, Pref.USER_PHONE_NUMBER, ""));
        pinView.setCursorVisible(true);
        pinView.setAnimationEnable(true);
        pinView.setCursorColor(getColor(R.color.colorAccent));
        pinView.setHideLineWhenFilled(false);

    }

    private void clicks() {
        nextBtn.setOnClickListener(v -> {

            String loginId = phone.getText().toString();
            String pin = pinView.getText().toString();
            if (!Validator.isValidPhoneNumber(loginId)) {
                AppMethods.showMessage(context, "Enter a valid number");
                return;
            }
            if (pin == null || pin.equals("") || pin.length() < 4) {
                AppMethods.showMessage(context, "Enter a valid PIN");
                return;
            }
            login(loginId, pin);
        });
        signup.setOnClickListener(v -> {
            Intent intent = new Intent(context, RegistrationStepOneActivity.class);
            startActivity(intent);
        });
    }

    public void login(String loginId, String pass) {
        progressDrawable.show();
        Map params = new HashMap<>();
        params.put("loginid", loginId);
        params.put("pin", pass);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.login(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<RegisterStep1Response>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppMethods.showMessage(context, "Oops..!! No internet connection");
                        AppLog.e("-------- " + throwable.getMessage());
                        progressDrawable.dismiss();

                    }

                    @Override
                    public void onNext(Response<RegisterStep1Response> objectResponse) {
                        RegisterStep1Response response = objectResponse.body();
                        progressDrawable.dismiss();

                        if (response.getStatus().equals(SUCCESS)) {
                            Pref.writeInteger(context, Pref.PREF_USER_ID, response.getUserID());
                            Intent intent = new Intent(context, DashboardActivity.class);
                            Gson gson = new Gson();
                            String json = gson.toJson(response);
                            Pref.writeString(context, Pref.INTENT_USER_LOGIN_USER_DATA, json);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            Pref.writeString(context, Pref.USER_PHONE_NUMBER, response.getPerson().getPhone());

                            startActivity(intent);
                        } else {
                            AppMethods.showMessage(context, response.userStatus);
                        }
                    }
                });
    }
}