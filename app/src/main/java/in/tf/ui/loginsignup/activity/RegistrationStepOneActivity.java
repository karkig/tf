package in.tf.ui.loginsignup.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.model.registerlogin.PhoneRequest;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.network.APIService;
import in.tf.network.RequestResponses;
import in.tf.receiver.SMSListener;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import in.tf.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.tf.constants.Constants.SUCCESS;
import static in.tf.constants.IntentConstants.INTENT_PHONE_NUMBER;
import static in.tf.constants.IntentConstants.INTENT_USER_ID;

public class RegistrationStepOneActivity extends AppCompatActivity {

    @BindView(R.id.nextBtn)
    Button nextBtn;
    @BindView(R.id.phone)
    AutoCompleteTextView phone;
    RegistrationStepOneActivity context;
    ProgressDialog progressDrawable;
    private static int REQUEST_CODE_READ_SMS = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regisration_step_one);
        context = this;
        ButterKnife.bind(context);
        clicks();
        init();
    }

    private void init() {
        ActivityCompat.requestPermissions(context, new String[]{android.Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_READ_SMS);
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);

    }

    private void clicks() {
        nextBtn.setOnClickListener(v -> {
            if (!phone.getText().toString().equals("") && Validator.isValidPhoneNumber(phone.getText())) {
                AppMethods.hideKeyboard(context);
                register(phone.getText().toString());
            } else {
                AppMethods.showMessage(context, "Please enter a valid number");
            }
        });
    }

    public void register(String phone) {
        PhoneRequest request = new PhoneRequest();
        request.setLoginid(phone);
        progressDrawable.show();

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.registerPhoneNumber(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<RegisterStep1Response>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("RegistrationStepOneActivity =======> " + throwable.getMessage());
                        progressDrawable.dismiss();
                    }

                    @Override
                    public void onNext(Response<RegisterStep1Response> objectResponse) {
                        progressDrawable.dismiss();
                        RegisterStep1Response response = objectResponse.body();
                        if (response.getStatus().equals(SUCCESS)) {
                            Pref.writeString(context, Pref.USER_PHONE_NUMBER, response.getPerson().getPhone());
                            AppLog.e("phone =========>" + response.getPerson().getPhone());
                            Intent intent = new Intent(context, OTPActivity.class);
                            intent.putExtra(INTENT_PHONE_NUMBER, response.getPerson().phone);
                            intent.putExtra(INTENT_USER_ID, response.userID.intValue() + "");
                            AppLog.e("RegistrationStepOneActivity =======> Register login Id", response.userID + "");
                            startActivity(intent);
                        } else {
                            AppMethods.showMessage(context, response.userStatus);
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        SMSListener.unbindListener();
        super.onDestroy();
    }
}


