package in.tf.ui.loginsignup.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chaos.view.PinView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.constants.IntentConstants;
import in.tf.listeners.OTPListener;
import in.tf.model.registerlogin.PhoneRequest;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.network.APIService;
import in.tf.network.RequestResponses;
import in.tf.receiver.SMSListener;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static in.tf.constants.Constants.SUCCESS;
import static in.tf.constants.IntentConstants.INTENT_PHONE_NUMBER;
import static in.tf.constants.IntentConstants.INTENT_USER_ID;


/**
 * A login screen that offers login via email/password.
 */
public class OTPActivity extends AppCompatActivity {

    @BindView(R.id.resendBtn)
    TextView resendBtn;
    @BindView(R.id.confirmOtpBtn)
    Button confirmOtpBtn;
    @BindView(R.id.txtPhoneNo)
    TextView txtPhoneNo;
    @BindView(R.id.firstPinView)
    PinView firstPinView;

    OTPActivity context;
    String phone;
    String userId;
    ProgressDialog progressDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        context = this;
        ButterKnife.bind(context);
        clicks();
        init();
    }

    private void init() {
        Intent intent = getIntent();
        if (null != intent) {
            phone = intent.getStringExtra(IntentConstants.INTENT_PHONE_NUMBER);
            userId = intent.getStringExtra(IntentConstants.INTENT_USER_ID);
            txtPhoneNo.setText(phone);
            AppLog.e(" OTPActivity =======> " + userId);
        } else {
            finish();
        }
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
        firstPinView.setCursorVisible(true);
        firstPinView.setAnimationEnable(true);
        firstPinView.setCursorColor(getColor(R.color.colorAccent));
        firstPinView.setHideLineWhenFilled(false);
        SMSListener.bindListener(extractedOTP -> {
            firstPinView.setText(extractedOTP);
            verifyOTP(extractedOTP);
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void clicks() {
        confirmOtpBtn.setOnClickListener(v -> {
            if (firstPinView.getText().toString().length() == 4) {
                verifyOTP(firstPinView.getText().toString());
            } else {
                AppMethods.showMessage(context, "Invalid OTP");
            }
        });
        resendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendOTP(phone);
            }
        });
    }

    private void moveToFullRegistration() {
        Intent intent = new Intent(context, RegistrationStep2Activity.class);
        intent.putExtra(INTENT_USER_ID, userId + "");
        startActivity(intent);
    }

    public void verifyOTP(String otp) {
        progressDrawable.show();
        AppMethods.hideKeyboard(context);
        Map params = new HashMap<>();
        params.put("value", otp);
        params.put("key", phone);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.verifyOTP(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<RegisterStep1Response>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppMethods.showMessage(context, "Invalid OTP");
                        AppLog.e("-------- " + throwable.getMessage());
                        progressDrawable.dismiss();
                    }

                    @Override
                    public void onNext(Response<RegisterStep1Response> objectResponse) {
                        RegisterStep1Response response = objectResponse.body();
                        progressDrawable.dismiss();
                        if (response.getStatus().equals(SUCCESS)) {
                            moveToFullRegistration();
                        }
                    }
                });
    }

    public void resendOTP(String phone) {
        PhoneRequest request = new PhoneRequest();
        request.setLoginid(phone);
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.registerPhoneNumber(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<RegisterStep1Response>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<RegisterStep1Response> objectResponse) {
                    }
                });
    }
}