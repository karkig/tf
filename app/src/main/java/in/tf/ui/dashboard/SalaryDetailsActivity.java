package in.tf.ui.dashboard;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.constants.IntentConstants;

/**
 * A login screen that offers login via email/password.
 */
public class SalaryDetailsActivity extends AppCompatActivity {

    @BindView(R.id.seekBarGrossSalary)
    IndicatorSeekBar seekBarGrossSalary;
    @BindView(R.id.edtGrossSalary)
    EditText edtGrossSalary;
    @BindView(R.id.seekBarNetSalary)
    IndicatorSeekBar seekBarNetSalary;
    @BindView(R.id.edtNetSalary)
    EditText edtNetSalary;

    SalaryDetailsActivity context;
    String userId;
    ProgressDialog progressDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.include_salary_details);
        userId = getIntent().getStringExtra(IntentConstants.INTENT_USER_ID);
        context = this;
        ButterKnife.bind(context);
        clicks();
        init();
    }

    private void init() {
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
    }

    private void clicks() {



    }
}