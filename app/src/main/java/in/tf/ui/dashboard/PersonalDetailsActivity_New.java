package in.tf.ui.dashboard;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.adapters.Pager;
import in.tf.constants.IntentConstants;
import in.tf.model.personaldetails.PersonDetailRequest;
import in.tf.model.registerlogin.Person;
import in.tf.network.APIService;
import in.tf.network.CommonApi;
import in.tf.network.RequestResponses;
import in.tf.ui.verification.fragments.AddressFragment;
import in.tf.ui.verification.fragments.DocumentVerifyFragment;
import in.tf.ui.verification.fragments.PersonalDetailsFragment;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.FileManager;
import in.tf.util.Pref;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PersonalDetailsActivity_New extends AppCompatActivity {

    static final int VIDEO_VERIFY_REQUEST_CODE = 100;
    static final int IMAGE_VERIFY_REQUEST_CODE = 200;
    public static final int GALLERY_PICTURE = 300;
    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabsLayout)
    TabLayout tabsLayout;
    @BindView(R.id.fabButton)
    FloatingActionButton fabButton;
    PersonalDetailsActivity_New context;
    String userId;
    ProgressDialog progressDrawable;
    public static Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details_new);
        userId = getIntent().getStringExtra(IntentConstants.INTENT_USER_ID);
        context = this;
        ButterKnife.bind(context);
        clicks();
        init();
        setupViewPager();
        getPersonData();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void init() {
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
    }

    private void clicks() {
        btnBack.setOnClickListener(view -> finish()
        );
        fabButton.setOnClickListener(view -> {
            if (viewPager.getCurrentItem() == 0) {
                PersonalDetailsFragment frag = (PersonalDetailsFragment) viewPager
                        .getAdapter()
                        .instantiateItem(viewPager, viewPager.getCurrentItem());
                frag.updatePersonalDetails();
            } else if (viewPager.getCurrentItem() == 1) {
                AddressFragment frag = (AddressFragment) viewPager
                        .getAdapter()
                        .instantiateItem(viewPager, viewPager.getCurrentItem());
                frag.updateAddress();
            }
        });
        tabsLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    PersonalDetailsFragment frag = (PersonalDetailsFragment) viewPager
                            .getAdapter()
                            .instantiateItem(viewPager, viewPager.getCurrentItem());
                    frag.updateViewWithData(person);
                } else if (position == 1) {
                    AddressFragment frag = (AddressFragment) viewPager
                            .getAdapter()
                            .instantiateItem(viewPager, viewPager.getCurrentItem());
                    frag.updateViewWithData(person);

                }
                if (position == 2) {
                    fabButton.setVisibility(View.GONE);
                } else {
                    fabButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setupViewPager() {

        Pager adapter = new Pager(
                getSupportFragmentManager(), 3);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
        tabsLayout.setupWithViewPager(viewPager, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_VERIFY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String videoPath = FileManager.getVerificationVideoPath() + FileManager.VIDEO_FORMAT;

                if (viewPager.getCurrentItem() == 2) {
                    DocumentVerifyFragment frag = (DocumentVerifyFragment) viewPager
                            .getAdapter()
                            .instantiateItem(viewPager, viewPager.getCurrentItem());
                    frag.updateVideoView(videoPath);
                }

            }
        } else if (requestCode == IMAGE_VERIFY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                if (viewPager.getCurrentItem() == 2) {
                    String imagePath = FileManager.getVerificationImagePath() + FileManager.IMAGE_FORMAT;
                    DocumentVerifyFragment frag = (DocumentVerifyFragment) viewPager
                            .getAdapter()
                            .instantiateItem(viewPager, viewPager.getCurrentItem());
                    frag.updateImageView(imagePath);
                }

            }
        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            AppLog.e("====== called " + data.getData());
            if (data != null) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String selectedImagePath = c.getString(columnIndex);
                c.close();
                if (viewPager.getCurrentItem() == 0) {
                    PersonalDetailsFragment frag = (PersonalDetailsFragment) viewPager
                            .getAdapter()
                            .instantiateItem(viewPager, viewPager.getCurrentItem());
                    frag.updateProfilePic(selectedImagePath);
                    AppMethods.uploadImageProfileImage(context, Pref.readInteger(context, Pref.PREF_USER_ID) + "", selectedImagePath, "");
                }

            } else {
                Toast.makeText(getApplicationContext(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getPersonData() {
        AppMethods.hideKeyboard(context);
        PersonDetailRequest request = new PersonDetailRequest(Pref.readInteger(context, Pref.PREF_USER_ID));
        if (request == null) {
            return;
        }
        progressDrawable.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getPersonDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<Person>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                        progressDrawable.dismiss();

                    }

                    @Override
                    public void onNext(Response<Person> objectResponse) {
                        progressDrawable.dismiss();
                        person = objectResponse.body();
                        // initially update the once available
                        if (viewPager.getCurrentItem() == 0) {
                            PersonalDetailsFragment frag = (PersonalDetailsFragment) viewPager
                                    .getAdapter()
                                    .instantiateItem(viewPager, viewPager.getCurrentItem());
                            frag.updateViewWithData(person);
                        }
                    }
                });
    }

}