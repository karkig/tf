package in.tf.ui.dashboard;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.chaos.view.PinView;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.model.registerlogin.OTPMPinRequest;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.model.registerlogin.ResetMPinRequest;
import in.tf.network.APIService;
import in.tf.network.RequestResponses;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A login screen that offers login via email/password.
 */
public class ResetMpinActivity extends AppCompatActivity {

    @BindView(R.id.mPin)
    PinView mPin;
    @BindView(R.id.otp)
    PinView otp;
    @BindView(R.id.resetBtn)
    Button resetBtn;
    ResetMpinActivity context;
    ProgressDialog progressDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pin);
        context = this;
        ButterKnife.bind(context);
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
        resetBtn.setOnClickListener(view -> resetMpin());

    }

    @Override
    protected void onResume() {
        super.onResume();
        requestForOtp();

    }

    public void requestForOtp() {
        progressDrawable.show();
        String response = Pref.readString(context, Pref.INTENT_USER_LOGIN_USER_DATA, "");
        RegisterStep1Response data = new Gson().fromJson(response, RegisterStep1Response.class);
        OTPMPinRequest request = new OTPMPinRequest();
        request.setUserId(data.getUserGUID());
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.requestForOtp(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<String>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- EmploymentDetailsActivity updateIncomeDetails" + throwable.getMessage());
                        progressDrawable.dismiss();

                    }

                    @Override
                    public void onNext(Response<String> objectResponse) {
                        progressDrawable.dismiss();
                        AppMethods.showMessage(context, "Income details updated successfully.");
                    }
                });

    }

    public void resetMpin() {
        String userId = Pref.readInteger(context, Pref.PREF_USER_ID) + "";
        progressDrawable.show();
        ResetMPinRequest request = new ResetMPinRequest();
        request.setUserid(userId);
        request.setPIN(mPin.getText() + "");
        request.setRecentOpt(otp.getText() + "");
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.resetMpin(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<String>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- EmploymentDetailsActivity updateIncomeDetails" + throwable.getMessage());
                        progressDrawable.dismiss();

                    }

                    @Override
                    public void onNext(Response<String> objectResponse) {
                        progressDrawable.dismiss();
                        AppMethods.showMessage(context, "MPIN changed successfully.");
                        finish();
                    }
                });

    }


}