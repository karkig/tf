package in.tf.ui.dashboard;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.constants.IntentConstants;
import in.tf.model.common.FilterRequest;
import in.tf.model.common.FilterResponse;
import in.tf.model.common.MasterApiCommonResponse;
import in.tf.model.common.StateFilterResponse;
import in.tf.model.common.StateApiResponse;
import in.tf.model.organization.BankAccount;
import in.tf.model.organization.BankDetailsResponse;
import in.tf.model.organization.BankList;
import in.tf.model.organization.GetIncomeDetailsRequest;
import in.tf.model.organization.Organisation;
import in.tf.model.organization.OrganisationAddress;
import in.tf.model.organization.OrganisationDetailRequestResponse;
import in.tf.network.APIService;
import in.tf.network.CommonApi;
import in.tf.network.RequestResponses;
import in.tf.util.AppLog;
import in.tf.util.AppMethods;
import in.tf.util.Pref;
import in.tf.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A login screen that offers login via email/password.
 */
public class EmploymentDetailsActivity extends AppCompatActivity {

    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtName)
    AutoCompleteTextView edtName;
    @BindView(R.id.edtExpenditure)
    EditText edtExpenditure;
    @BindView(R.id.edtContactPerson)
    EditText edtContactPerson;
    @BindView(R.id.txtSalary)
    TextView txtIdDoc;
    @BindView(R.id.edtAddress2)
    EditText edtAddress2;
    @BindView(R.id.edtAddress1)
    EditText edtAddress1;
    @BindView(R.id.edtCity)
    AutoCompleteTextView edtCity;
    @BindView(R.id.edtPostal)
    EditText edtPostal;
    @BindView(R.id.edtState)
    AutoCompleteTextView edtState;
    @BindView(R.id.edtCountry)
    EditText edtCountry;
    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.edtCompanyPhone)
    EditText edtCompanyPhone;
    @BindView(R.id.seekBarGrossSalary)
    IndicatorSeekBar seekBarGrossSalary;
    @BindView(R.id.edtGrossSalary)
    EditText edtGrossSalary;
    @BindView(R.id.seekBarNetSalary)
    IndicatorSeekBar seekBarNetSalary;
    @BindView(R.id.edtNetSalary)
    EditText edtNetSalary;
    @BindView(R.id.edtBankName)
    AutoCompleteTextView edtBankName;
    @BindView(R.id.edtIFSCCode)
    EditText edtIFSCCode;
    @BindView(R.id.edtAccountNumber)
    EditText edtAccountNumber;
    @BindView(R.id.edtCompanyWebsite)
    EditText edtCompanyWebsite;
    @BindView(R.id.edtConfirmAccountNumber)
    EditText edtConfirmAccountNumber;
    @BindView(R.id.fabButton)
    FloatingActionButton fabButton;
    EmploymentDetailsActivity context;
    String userId;
    ProgressDialog progressDrawable;
    List<StateFilterResponse> stateList;
    List<BankList> bankList;
    List<FilterResponse> organizationList;

    ArrayAdapter stateAdapter;
    String selectedStateCode = "";

    ArrayAdapter organizationAdapter;
    Integer selectedOrganizationCode = 0;

    ArrayAdapter bankAdapter;
    Integer selectedBankCode = 0;

    List<FilterResponse> cityList;
    ArrayAdapter cityAdapter;
    Integer selectedCityCode = 0;
    OrganisationDetailRequestResponse organisationDetailRequestResponse;
    int addressId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employment_details);
        userId = getIntent().getStringExtra(IntentConstants.INTENT_USER_ID);
        context = this;
        ButterKnife.bind(context);
        clicks();
        init();
        loadPersonIncomeDetails();
        loadOrganizationList();
        loadStateFromServer();
        loadBankListFromServer();
    }

    private void init() {
        progressDrawable = new ProgressDialog(context);
        progressDrawable.setMessage("Please wait...");
        progressDrawable.setCancelable(false);
        progressDrawable.setCanceledOnTouchOutside(false);
    }


    private void clicks() {
        edtState.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (StateFilterResponse filterResponse : stateList) {
                if (filterResponse.getValue().equals(itemName)) {
                    selectedStateCode = filterResponse.getKey();
                    break;
                }
            }
            loadCityForPresentAddress(selectedStateCode);
        });
        edtCity.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (FilterResponse filterResponse : cityList) {
                if (filterResponse.getValue().equals(itemName)) {
                    selectedCityCode = filterResponse.getKey();
                    break;
                }
            }
        });
        edtBankName.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (BankList filterResponse : bankList) {
                if (filterResponse.getBankName().equals(itemName)) {
                    selectedBankCode = filterResponse.getBankID();
                    break;
                }
            }
        });
        edtName.setOnItemClickListener((adapterView, view, i, l) -> {
            String itemName = adapterView.getAdapter().getItem(i).toString();
            for (FilterResponse filterResponse : organizationList) {
                if (filterResponse.getValue().equals(itemName)) {
                    selectedOrganizationCode = filterResponse.getKey();
                    break;
                }
            }
        });
        btnBack.setOnClickListener(view -> finish());
        seekBarGrossSalary.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                /* called when the user is sliding the thumb */
                edtGrossSalary.setText(seekParams.seekBar.getProgress() + "");
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
                /* called when the sliding of thumb is started */
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                /* called when the sliding of thumb stops */
                edtGrossSalary.setText(seekBar.getProgress() + "");
            }
        });
        seekBarNetSalary.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                /* called when the user is sliding the thumb */
                edtNetSalary.setText(seekParams.seekBar.getProgress() + "");
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
                /* called when the sliding of thumb is started */
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                /* called when the sliding of thumb stops */
                edtNetSalary.setText(seekBar.getProgress() + "");
            }
        });
        fabButton.setOnClickListener(view -> {
            updateIncomeDetails();
        });
    }

    private OrganisationDetailRequestResponse createIncomeRequest() {
        OrganisationDetailRequestResponse organisationDetailRequest = new OrganisationDetailRequestResponse();

        organisationDetailRequest.setPersonID(Pref.readInteger(context, Pref.PREF_USER_ID));
        Organisation organisation = new Organisation();
        OrganisationAddress address = new OrganisationAddress();

        if (Validator.isValidString(edtName, context, "Enter valid company name ")) {
            organisation.setOrganisationName(edtName.getText().toString());
            organisation.setOrganisationid(selectedOrganizationCode);
        } else {
            return null;
        }

        if (Validator.isValidString(edtContactPerson, context, "Enter valid contact person number ")) {
            organisation.setContactPerson(edtContactPerson.getText().toString());
        } else {
            return null;
        }
        organisation.setOrganisationWebsite(edtCompanyWebsite.getText().toString());

       /* if (Validator.isValidURL(edtCompanyWebsite.getText().toString())) {
            organisation.setOrganisationWebsite(edtCompanyWebsite.getText().toString());
        } else {
            AppMethods.showMessage(context, "Enter valid website");
            return null;
        }*/
        if (Validator.isValidString(edtCompanyPhone, context, "Enter valid contact person number ")) {
            organisation.setOrganisationPhone(edtCompanyPhone.getText().toString());
        } else {
            return null;
        }
        if (Validator.isValidString(edtEmail, context, "Enter valid email ")) {
            organisation.setOrganisationEmail(edtEmail.getText().toString());
        } else {
            return null;
        }

        if (Validator.isValidString(edtAddress1, context, "Enter valid Address ")) {
            address.setAddressLine1(edtAddress1.getText().toString());
        } else {
            return null;
        }
        if (Validator.isValidString(edtAddress2, context, "Enter valid land mark ")) {
            address.setAddressRemark(edtAddress2.getText().toString());
        } else {
            return null;
        }

        if (Validator.isValidString(edtState, context, "Enter valid state")) {
            address.setState(edtState.getText().toString());

        } else {
            return null;
        }
        if (Validator.isValidString(edtCity, context, "Enter valid city")) {
            address.setCity(edtCity.getText().toString());
        } else {
            return null;
        }
        if (Validator.isValidString(edtPostal, context, "Enter valid postal code")) {
            address.setPostCode(edtPostal.getText().toString());
        } else {
            return null;
        }


        organisationDetailRequest.setExpenditure("");
        if (Validator.isValidString(edtGrossSalary, context, "Enter your valid gross salary")) {
            organisationDetailRequest.setGrossSalary(edtGrossSalary.getText().toString());
        } else {
            return null;
        }

        if (Validator.isValidString(edtExpenditure, context, "Enter your valid expenditure of per month")) {
            organisationDetailRequest.setExpenditure(edtExpenditure.getText().toString());
        } else {
            return null;
        }

        if (Validator.isValidString(edtNetSalary, context, "Enter your valid net salary")) {
            organisationDetailRequest.setNetPay(edtNetSalary.getText().toString());
        } else {
            return null;
        }
        if (!selectedStateCode.equals("")) {
            address.setStateID(selectedStateCode);
        } else {
            AppMethods.showMessage(context, "Please reselect state");

        }
        if (!selectedCityCode.equals("")) {
            address.setCityID(selectedCityCode + "");
        } else {
            AppMethods.showMessage(context, "Please reselect city");
        }

        address.setCountryID("IN");
        address.setAddressID(addressId);
        organisation.setOrganisationAddress(address);
        organisationDetailRequest.setOrganisation(organisation);
        BankAccount bankAccount = new BankAccount();

        if (Validator.isValidString(edtBankName, context, "Enter your valid bank name")) {
            bankAccount.setBankName(edtBankName.getText().toString());
        } else {
            return null;
        }
        if (Validator.isValidString(edtIFSCCode, context, "Enter your valid IFSC")) {
            bankAccount.setiFSC(edtIFSCCode.getText().toString());
        } else {
            return null;
        }

        if (Validator.isValidString(edtAccountNumber, context, "Enter your valid account holder name")) {
            if (Validator.isValidString(edtConfirmAccountNumber.getText().toString())) {
                String conAnt = edtConfirmAccountNumber.getText().toString();
                String acont = edtAccountNumber.getText().toString();
                if (acont.equals(conAnt)) {
                    bankAccount.setAccountNumber(Long.parseLong(edtConfirmAccountNumber.getText().toString()));
                } else {
                    AppMethods.showMessage(context, "Account number mismatch");
                }
            }
        } else {
            return null;
        }
        bankAccount.setBankID(selectedBankCode);
        organisationDetailRequest.setBankAccount(bankAccount);

        return organisationDetailRequest;
    }

    public void updateIncomeDetails() {
        AppMethods.hideKeyboard(context);
        OrganisationDetailRequestResponse request = createIncomeRequest();
        if (request == null) {
            return;
        }
        progressDrawable.setMessage("Updating details...");
        progressDrawable.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.updateIncomeDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<OrganisationDetailRequestResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- EmploymentDetailsActivity updateIncomeDetails" + throwable.getMessage());
                        progressDrawable.dismiss();

                    }

                    @Override
                    public void onNext(Response<OrganisationDetailRequestResponse> objectResponse) {
                        progressDrawable.dismiss();
                        AppMethods.showMessage(context, "Income details updated successfully.");
                    }
                });

    }

    public void loadPersonIncomeDetails() {
        AppMethods.hideKeyboard(context);
        GetIncomeDetailsRequest request = new GetIncomeDetailsRequest(Pref.readInteger(context, Pref.PREF_USER_ID));
        if (request == null) {
            return;
        }
        progressDrawable.setMessage("Fetching details...");
        progressDrawable.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getIncomeDetails(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<OrganisationDetailRequestResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- EmploymentDetailsActivity loadPersonIncomeDetails" + throwable.getMessage());
                        progressDrawable.dismiss();
                    }

                    @Override
                    public void onNext(Response<OrganisationDetailRequestResponse> objectResponse) {
                        progressDrawable.dismiss();
                        if (objectResponse.body().getCode() == 0) {
                            AppMethods.showMessage(context, "Please add your income details");
                        } else {
                            organisationDetailRequestResponse = objectResponse.body();
                            updateViewByData();
                        }

                    }
                });
    }

    private void updateViewByData() {
        try {
            if (organisationDetailRequestResponse != null) {
                edtGrossSalary.setText(organisationDetailRequestResponse.getGrossSalary());
                seekBarGrossSalary.setProgress(Float.parseFloat(organisationDetailRequestResponse.getGrossSalary()));
                edtNetSalary.setText(organisationDetailRequestResponse.getNetPay());
                seekBarNetSalary.setProgress(Float.parseFloat(organisationDetailRequestResponse.getNetPay()));
                edtExpenditure.setText(organisationDetailRequestResponse.getExpenditure());
                Organisation organisation = organisationDetailRequestResponse.getOrganisation();
                if (organisation != null) {
                    edtCompanyWebsite.setText(organisation.getOrganisationWebsite());
                    edtName.setText(organisation.getOrganisationName());
                    selectedOrganizationCode = organisation.getOrganisationid();
                    edtContactPerson.setText(organisation.getContactPerson());
                    edtCompanyPhone.setText(organisation.getOrganisationPhone());
                    edtEmail.setText(organisation.getOrganisationEmail());
                    OrganisationAddress address = organisation.getOrganisationAddress();
                    if (address != null) {
                        addressId = address.getAddressID();
                        edtAddress1.setText(address.getAddressLine1());
                        edtAddress2.setText(address.getAddressRemark());
                        edtCountry.setText(address.getCountryID());
                        edtState.setText(address.getState());
                        selectedStateCode = address.getStateID();
                        selectedCityCode = Integer.parseInt(address.getCityID());
                        edtCity.setText(address.getCity());
                        edtPostal.setText(address.getPostCode());
                    }
                }
                BankAccount bankDetails = organisationDetailRequestResponse.getBankAccount();
                if (bankDetails != null) {
                    edtAccountNumber.setText(String.valueOf(bankDetails.getAccountNumber()));
                    edtConfirmAccountNumber.setText(String.valueOf(bankDetails.getAccountNumber()));
                    edtIFSCCode.setText(bankDetails.getiFSC());
                    edtBankName.setText(bankDetails.getBankName());
                    selectedBankCode = bankDetails.getBankID();
                }
            }
        } catch (Exception e) {
            AppLog.e("-------- exception " + e.getMessage());
        }


    }

    private void loadStateFromServer() {
        progressDrawable.setMessage("Loading states...");
        progressDrawable.show();
        CommonApi.getStateList(context, new FilterRequest("IN"), data -> {
            stateList = ((StateApiResponse) data).list;
            ArrayList<String> filterList = new ArrayList<>();
            for (StateFilterResponse item : stateList) {
                filterList.add(item.getValue());
            }
            stateAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            edtState.setAdapter(stateAdapter);
            progressDrawable.dismiss();

        });
    }

    private void loadBankListFromServer() {

        CommonApi.getBankDetails(context, new FilterRequest(""), data -> {
            bankList = ((BankDetailsResponse) data).getBankList();
            ArrayList<String> filterList = new ArrayList<>();
            for (BankList item : bankList) {
                filterList.add(item.getBankName());
            }
            bankAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            edtBankName.setAdapter(bankAdapter);
        });
    }

    private void loadOrganizationList() {

        CommonApi.getOrganizationList(context, new FilterRequest(""), data -> {
            organizationList = ((MasterApiCommonResponse) data).getList();
            ArrayList<String> filterList = new ArrayList<>();
            for (FilterResponse item : organizationList) {
                filterList.add(item.getValue());
            }
            organizationAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            edtName.setAdapter(organizationAdapter);
        });
    }

    private void loadCityForPresentAddress(String stateCode) {
        progressDrawable.setMessage("Fetching cities...");
        progressDrawable.show();

        CommonApi.getCityList(context, new FilterRequest(stateCode), data -> {
            cityList = ((MasterApiCommonResponse) data).list;
            ArrayList<String> filterList = new ArrayList<>();
            for (FilterResponse item : cityList) {
                filterList.add(item.getValue());
            }
            cityAdapter = new
                    ArrayAdapter(context, android.R.layout.simple_list_item_1, filterList);
            edtCity.setAdapter(cityAdapter);
            progressDrawable.dismiss();

        });
    }
}