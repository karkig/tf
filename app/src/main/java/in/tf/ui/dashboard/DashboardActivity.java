package in.tf.ui.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.constants.IntentConstants;
import in.tf.model.common.FilterRequest;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.network.CommonApi;
import in.tf.network.CommonApiCallback;
import in.tf.ui.loginsignup.activity.LoginActivity;
import in.tf.util.Pref;

public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.pieChart)
    PieChart pieChart;
    @BindView(R.id.menuIcon)
    TextView menuIcon;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.logout)
    LinearLayout logout;
    @BindView(R.id.personalDetail)
    LinearLayout personalDetail;
    @BindView(R.id.empDetail)
    LinearLayout empDetail;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.fab1)
    FloatingActionButton fab1;
    @BindView(R.id.fab2)
    FloatingActionButton fab2;
    DashboardActivity context;
    RegisterStep1Response data;
    boolean isFABOpen = false;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        context = this;
        ButterKnife.bind(context);
        init();
        clicks();
        loadBasicApi();
    }

    private void loadBasicApi() {

    }

    private void init() {
        Intent intent = getIntent();
        if (intent != null) {
            String json = Pref.readString(context, Pref.INTENT_USER_LOGIN_USER_DATA, "");
            data = new Gson().fromJson(json, RegisterStep1Response.class);
        }
        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

    }

    private void closeDrawer() {
        new Handler().postDelayed(() -> {
            drawerLayout.closeDrawer(GravityCompat.START);

        }, 50);
    }

    private void clicks() {
        menuIcon.setOnClickListener(view -> {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);

            }
        });
        fab.setOnClickListener(view -> {
            animateFAB();
        });
        personalDetail.setOnClickListener(view -> {
            Intent intent = new Intent(context, PersonalDetailsActivity_New.class);
            startActivity(intent);
            closeDrawer();
        });
        empDetail.setOnClickListener(view -> {
            Intent intent = new Intent(context, EmploymentDetailsActivity.class);
            startActivity(intent);
            closeDrawer();

        });
        fab2.setOnClickListener(view -> {
            Intent intent = new Intent(context, ResetMpinActivity.class);
            startActivity(intent);
        });
        logout.setOnClickListener(view -> {
            Intent intent = new Intent(context, LoginActivity.class);
            intent.putExtra("finish", true); // if you are checking for this in your other Activities
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            closeDrawer();

        });

    }

    public void animateFAB() {

        if (isFABOpen) {
            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFABOpen = false;

        } else {
            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFABOpen = true;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initChart();
    }

    private void initChart() {
        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(2600.5f, getString(R.string.Rs) + "Graphic Era"));
        entries.add(new PieEntry(2226.7f, getString(R.string.Rs) + " DPS"));
        entries.add(new PieEntry(3220.8f, getString(R.string.Rs) + " Available"));
        PieDataSet set = new PieDataSet(entries, "");
        set.setSliceSpace(.5f);
        int colors[] = new int[]{
                Color.parseColor("#F7A06D"),
                Color.parseColor("#A3DCFE"),
                Color.parseColor("#BF90EA")};
        set.setColors(colors);
        PieData data = new PieData(set);
        pieChart.setData(data);
        pieChart.invalidate(); // refresh
        pieChart.setEntryLabelTextSize(7);
        pieChart.setCenterText("Expenditure");
        pieChart.setCenterTextSize(10);
        pieChart.setHoleRadius(40);
        pieChart.setTransparentCircleRadius(45);
        pieChart.setCenterTextColor(R.color.textGreyCColor);
        Description description = new Description();
        description.setText("");
        pieChart.setDescription(description);
    }
}
