package in.tf.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.tf.ui.verification.fragments.AddressFragment;
import in.tf.ui.verification.fragments.DocumentVerifyFragment;
import in.tf.ui.verification.fragments.PersonalDetailsFragment;

public class Pager extends FragmentStatePagerAdapter {

    int tabCount;

    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Your Details";
            case 1:
                return "Address";
            case 2:
                return "Documents";

            default:
                return null;
        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                PersonalDetailsFragment personalDetailFragments = new PersonalDetailsFragment();
                return personalDetailFragments;
            case 1:
                AddressFragment addressFragment = new AddressFragment();
                return addressFragment;

            case 2:
                DocumentVerifyFragment documentVerifyFragment = new DocumentVerifyFragment();
                return documentVerifyFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}