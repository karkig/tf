package in.tf.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import in.tf.R;
import in.tf.model.common.FilterResponse;

public class DocSpinnerAdapter extends ArrayAdapter<FilterResponse> {

    LayoutInflater flater;
    List<FilterResponse> list;

    public DocSpinnerAdapter(Activity context, List<FilterResponse> list) {

        super(context, R.layout.spinner_textview, list);
        flater = context.getLayoutInflater();
        this.list = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = flater.inflate(R.layout.spinner_textview, null, true);
        rootViewNow(rowView, position);
        return rowView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = flater.inflate(R.layout.spinner_textview, null, true);

        return rootViewNow(rowView, position);
    }

    public View rootViewNow(View view, int position) {
        TextView spinnerItem = view.findViewById(R.id.spinnerItem);
        spinnerItem.setText(list.get(position).getValue());
        return view;
    }
}