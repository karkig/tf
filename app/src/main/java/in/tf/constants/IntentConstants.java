package in.tf.constants;

/**
 * Created by godoctor on 21/3/17.
 */

public class IntentConstants {
    public static final String INTENT_IMAGE_PATH = "INTENT_IMAGE_PATH";
    public static final String INTENT_VIDEO_PATH = "INTENT_VIDEO_PATH";
    public static final String INTENT_PHONE_NUMBER = "INTENT_PHONE_NUMBER";
    public static final String INTENT_USER_ID = "INTENT_USER_ID";
    public static final String INTENT_DOC_TYPE="INTENT_DOC_TYPE";

}
