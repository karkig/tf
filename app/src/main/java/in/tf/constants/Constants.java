package in.tf.constants;

import in.tf.config.ProjectConfig;
import in.tf.config.ProjectEnvironment;

/**
 * Created by godoctor on 21/3/17.
 */

public class Constants {
    public static String BASE_URL;
    public static final int NORMAL_REQUEST = 0;
    public static final int IMAGE_REQUEST = 1;
    public static final String SIMPLE_DATE_REGEX = "dd/MM/yyyy', 'hh:mm a";
    public static final String DATABASE_NAME = "english_updates";
    public static final String SUCCESS = "Success";
    public static Integer selctedDocTypeId = 0;


    /**
     *   Do not change the server url from here. To change goto ProjectConfig.java and change the project environment to
     *   ProjectEnvironment.STAG, ProjectEnvironment.PROD, ProjectEnvironment.DEV
     */
    static {
        ProjectEnvironment env = ProjectConfig.getEnv();
        if (env.environment.equals(ProjectEnvironment.DEV.environment)) {
            BASE_URL = "http://credilo.apphb.com/api/sandbox/";
        } else if (env.environment.equals(ProjectEnvironment.STAG.environment)) {
        } else if (env.environment.equals(ProjectEnvironment.PROD.environment)) {
            BASE_URL = "http://credilo.apphb.com/api/sandbox/";
        }
    }
}
