package in.tf.model.personaldetails.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAddressRequest {

    @SerializedName("personID")
    @Expose
    public Integer personID;
    @SerializedName("permanentAddress")
    @Expose
    public PermanentAddressRequest permanentAddress;
    @SerializedName("correspondenceAddress")
    @Expose
    public CorrespondenceAddressRequest correspondenceAddress;

    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public PermanentAddressRequest getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(PermanentAddressRequest permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public CorrespondenceAddressRequest getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(CorrespondenceAddressRequest correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }
}