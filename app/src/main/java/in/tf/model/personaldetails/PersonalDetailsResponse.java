package in.tf.model.personaldetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonalDetailsResponse {

    @SerializedName("personID")
    @Expose
    public Integer personID;
    @SerializedName("personGUID")
    @Expose
    public Object personGUID;
    @SerializedName("fullName")
    @Expose
    public String fullName;
    @SerializedName("dateOfBirth")
    @Expose
    public String dateOfBirth;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("emailVerified")
    @Expose
    public Boolean emailVerified;
    @SerializedName("phoneVerified")
    @Expose
    public Boolean phoneVerified;
    @SerializedName("imageID")
    @Expose
    public Integer imageID;
    @SerializedName("aadharNumber")
    @Expose
    public String aadharNumber;
    @SerializedName("panNumber")
    @Expose
    public String panNumber;
    @SerializedName("aadharNumberVerified")
    @Expose
    public Boolean aadharNumberVerified;
    @SerializedName("panNumberVerified")
    @Expose
    public Boolean panNumberVerified;
    @SerializedName("permanentAddress")
    @Expose
    public Object permanentAddress;
    @SerializedName("correspondenceAddress")
    @Expose
    public Object correspondenceAddress;
    @SerializedName("personVerified")
    @Expose
    public Boolean personVerified;

}