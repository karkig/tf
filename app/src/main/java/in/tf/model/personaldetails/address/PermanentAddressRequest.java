package in.tf.model.personaldetails.address;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PermanentAddressRequest {

    @SerializedName("addressID")
    @Expose
    public Integer addressID;
    @SerializedName("addressType")
    @Expose
    public Integer addressType;
    @SerializedName("addressLine1")
    @Expose
    public String addressLine1;
    @SerializedName("addressLine2")
    @Expose
    public String addressLine2;
    @SerializedName("cityID")
    @Expose
    public String cityID;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("stateID")
    @Expose
    public String stateID;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("countryID")
    @Expose
    public String countryID;
    @SerializedName("postCode")
    @Expose
    public String postCode;
    @SerializedName("addressRemark")
    @Expose
    public String addressRemark;

    public Integer getAddressID() {
        return addressID;
    }

    public void setAddressID(Integer addressID) {
        this.addressID = addressID;
    }

    public Integer getAddressType() {
        return addressType;
    }

    public void setAddressType(Integer addressType) {
        this.addressType = addressType;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getAddressRemark() {
        return addressRemark;
    }

    public void setAddressRemark(String addressRemark) {
        this.addressRemark = addressRemark;
    }
}