package in.tf.model.personaldetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PersonDetailRequest {

    @SerializedName("personID")
    @Expose
    public Integer personID;

    public PersonDetailRequest(Integer personID) {
        this.personID = personID;
    }
}