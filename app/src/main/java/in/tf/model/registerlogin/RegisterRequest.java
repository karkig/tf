package in.tf.model.registerlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest  {

    @SerializedName("userid")
    @Expose
    public String userid;
    @SerializedName("person")
    @Expose
    public Person person;
    @SerializedName("PIN")
    @Expose
    public String pIN;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getpIN() {
        return pIN;
    }

    public void setpIN(String pIN) {
        this.pIN = pIN;
    }
}