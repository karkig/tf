package in.tf.model.registerlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetMPinRequest {

    @SerializedName("userid")
    @Expose
    String userid;

    @SerializedName("PIN")
    @Expose
    String PIN;

    @SerializedName("RecentOTP")
    @Expose
    String recentOpt;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPIN() {
        return PIN;
    }

    public void setPIN(String PIN) {
        this.PIN = PIN;
    }

    public String getRecentOpt() {
        return recentOpt;
    }

    public void setRecentOpt(String recentOpt) {
        this.recentOpt = recentOpt;
    }
}
