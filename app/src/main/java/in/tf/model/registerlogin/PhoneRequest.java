package in.tf.model.registerlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneRequest {

    @SerializedName("loginid")
    @Expose
    public String loginid;

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }
}