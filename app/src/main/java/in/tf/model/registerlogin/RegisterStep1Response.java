package in.tf.model.registerlogin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterStep1Response  {

    @SerializedName("userID")
    @Expose
    public Integer userID;
    @SerializedName("userType")
    @Expose
    public Integer userType;
    @SerializedName("userGUID")
    @Expose
    public String userGUID;
    @SerializedName("loginID")
    @Expose
    public String loginID;
    @SerializedName("PIN")
    @Expose
    public String pIN;
    @SerializedName("emailVerified")
    @Expose
    public Boolean emailVerified;
    @SerializedName("phoneVerified")
    @Expose
    public Boolean phoneVerified;
    @SerializedName("active")
    @Expose
    public Boolean active;
    @SerializedName("isStudent")
    @Expose
    public Boolean isStudent;
    @SerializedName("info")
    @Expose
    public String info;
    @SerializedName("userStatus")
    @Expose
    public String userStatus;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("person")
    @Expose
    public Person person;

    public Integer getUserID() {
        return userID;
    }

    public Integer getUserType() {
        return userType;
    }

    public String getUserGUID() {
        return userGUID;
    }

    public String getLoginID() {
        return loginID;
    }


    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public Boolean getPhoneVerified() {
        return phoneVerified;
    }

    public Boolean getActive() {
        return active;
    }

    public Boolean getStudent() {
        return isStudent;
    }



    public String getStatus() {
        return status;
    }

    public Person getPerson() {
        return person;
    }


}