package in.tf.model.registerlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PermanentAddress {

    @SerializedName("addressID")
    @Expose
    public Integer addressID;
    @SerializedName("addressGUID")
    @Expose
    public String addressGUID;
    @SerializedName("addressLine1")
    @Expose
    public String addressLine1;
    @SerializedName("addressLine2")
    @Expose
    public String addressLine2;
    @SerializedName("cityID")
    @Expose
    public Integer cityID;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("postCode")
    @Expose
    public Object postCode;
    @SerializedName("stateID")
    @Expose
    public String stateID;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("countryID")
    @Expose
    public String countryID;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("addressType")
    @Expose
    public Integer addressType;
    @SerializedName("addressRemark")
    @Expose
    public String addressRemark;

    public Integer getAddressID() {
        return addressID;
    }

    public void setAddressID(Integer addressID) {
        this.addressID = addressID;
    }

    public String getAddressGUID() {
        return addressGUID;
    }

    public void setAddressGUID(String addressGUID) {
        this.addressGUID = addressGUID;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public Integer getCityID() {
        return cityID;
    }

    public void setCityID(Integer cityID) {
        this.cityID = cityID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Object getPostCode() {
        return postCode;
    }

    public void setPostCode(Object postCode) {
        this.postCode = postCode;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getAddressType() {
        return addressType;
    }

    public void setAddressType(Integer addressType) {
        this.addressType = addressType;
    }

    public String getAddressRemark() {
        return addressRemark;
    }

    public void setAddressRemark(String addressRemark) {
        this.addressRemark = addressRemark;
    }
}