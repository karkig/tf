package in.tf.model.registerlogin;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Person {


    @SerializedName("personID")
    @Expose
    public Integer personID;
    @SerializedName("personGUID")
    @Expose
    public String personGUID;
    @SerializedName("fullName")
    @Expose
    public String fullName;
    @SerializedName("dateOfBirth")
    @Expose
    public String dateOfBirth;
    @SerializedName("gender")
    @Expose
    public Object gender;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("emailVerified")
    @Expose
    public Boolean emailVerified;
    @SerializedName("phoneVerified")
    @Expose
    public Boolean phoneVerified;
    @SerializedName("imageID")
    @Expose
    public Integer imageID;
    @SerializedName("aadharNumber")
    @Expose
    public String aadharNumber;
    @SerializedName("panNumber")
    @Expose
    public String panNumber;
    @SerializedName("aadharNumberVerified")
    @Expose
    public Boolean aadharNumberVerified;
    @SerializedName("panNumberVerified")
    @Expose
    public Boolean panNumberVerified;
    @SerializedName("permanentAddress")
    @Expose
    public PermanentAddress permanentAddress;
    @SerializedName("correspondenceAddress")
    @Expose
    public CorrespondenceAddress correspondenceAddress;
    @SerializedName("personVerified")
    @Expose
    public Boolean personVerified;

    @SerializedName("imageURL")
    @Expose
    String imageURL;

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public String getPersonGUID() {
        return personGUID;
    }

    public void setPersonGUID(String personGUID) {
        this.personGUID = personGUID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public Boolean getPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(Boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public Integer getImageID() {
        return imageID;
    }

    public void setImageID(Integer imageID) {
        this.imageID = imageID;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public Boolean getAadharNumberVerified() {
        return aadharNumberVerified;
    }

    public void setAadharNumberVerified(Boolean aadharNumberVerified) {
        this.aadharNumberVerified = aadharNumberVerified;
    }

    public Boolean getPanNumberVerified() {
        return panNumberVerified;
    }

    public void setPanNumberVerified(Boolean panNumberVerified) {
        this.panNumberVerified = panNumberVerified;
    }

    public PermanentAddress getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(PermanentAddress permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public CorrespondenceAddress getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(CorrespondenceAddress correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public Boolean getPersonVerified() {
        return personVerified;
    }

    public void setPersonVerified(Boolean personVerified) {
        this.personVerified = personVerified;
    }
}