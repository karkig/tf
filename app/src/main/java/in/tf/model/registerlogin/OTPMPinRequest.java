package in.tf.model.registerlogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTPMPinRequest {

    @SerializedName("userguid")
    @Expose
    String userguid;

    public String getUserId() {
        return userguid;
    }

    public void setUserId(String userId) {
        this.userguid = userId;
    }
}
