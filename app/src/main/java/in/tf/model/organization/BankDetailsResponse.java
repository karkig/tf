package in.tf.model.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import in.tf.model.BaseResponse;

public class BankDetailsResponse extends BaseResponse{

    @SerializedName("list")
    @Expose
    public List<BankList> bankList = new ArrayList<>();

    public List<BankList> getBankList() {
        return bankList;
    }

    public void setBankList(List<BankList> bankList) {
        this.bankList = bankList;
    }
}