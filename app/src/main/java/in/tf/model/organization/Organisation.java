package in.tf.model.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Organisation {

    @SerializedName("organisationid")
    @Expose
    public Integer organisationid;
    @SerializedName("organisationAddress")
    @Expose
    public OrganisationAddress organisationAddress;
    @SerializedName("organisationName")
    @Expose
    public String organisationName;
    @SerializedName("industry")
    @Expose
    public String industry;
    @SerializedName("organisationType")
    @Expose
    public String organisationType;
    @SerializedName("contactPerson")
    @Expose
    public String contactPerson;
    @SerializedName("organisationEmail")
    @Expose
    public String organisationEmail;
    @SerializedName("organisationPhone")
    @Expose
    public String organisationPhone;
    @SerializedName("organisationWebsite")
    @Expose
    public String organisationWebsite;

    public Integer getOrganisationid() {
        return organisationid;
    }

    public void setOrganisationid(Integer organisationid) {
        this.organisationid = organisationid;
    }

    public OrganisationAddress getOrganisationAddress() {
        return organisationAddress;
    }

    public void setOrganisationAddress(OrganisationAddress organisationAddress) {
        this.organisationAddress = organisationAddress;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getOrganisationType() {
        return organisationType;
    }

    public void setOrganisationType(String organisationType) {
        this.organisationType = organisationType;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getOrganisationEmail() {
        return organisationEmail;
    }

    public void setOrganisationEmail(String organisationEmail) {
        this.organisationEmail = organisationEmail;
    }

    public String getOrganisationPhone() {
        return organisationPhone;
    }

    public void setOrganisationPhone(String organisationPhone) {
        this.organisationPhone = organisationPhone;
    }

    public String getOrganisationWebsite() {
        return organisationWebsite;
    }

    public void setOrganisationWebsite(String organisationWebsite) {
        this.organisationWebsite = organisationWebsite;
    }
}