package in.tf.model.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankList {

    @SerializedName("BankID")
    @Expose
    public Integer bankID;
    @SerializedName("IsActive")
    @Expose
    public Boolean isActive;
    @SerializedName("BankName")
    @Expose
    public String bankName;
    @SerializedName("LogoURL")
    @Expose
    public String logoURL;

    public Integer getBankID() {
        return bankID;
    }

    public void setBankID(Integer bankID) {
        this.bankID = bankID;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }
}