package in.tf.model.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetIncomeDetailsRequest {
    @SerializedName("personID")
    @Expose
    public Integer personID;

    public GetIncomeDetailsRequest(Integer personID) {
        this.personID = personID;
    }
}