package in.tf.model.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.tf.model.BaseResponse;

public class OrganisationDetailRequestResponse extends BaseResponse {

    @SerializedName("personID")
    @Expose
    public Integer personID;
    @SerializedName("organisation")
    @Expose
    public Organisation organisation;
    @SerializedName("grossSalary")
    @Expose
    public String grossSalary;
    @SerializedName("netPay")
    @Expose
    public String netPay;
    @SerializedName("expenditure")
    @Expose
    public String expenditure;
    @SerializedName("BankAccount")
    @Expose
    public BankAccount bankAccount;
    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public Organisation getOrganisation() {
        return organisation;
    }

    public void setOrganisation(Organisation organisation) {
        this.organisation = organisation;
    }

    public String getGrossSalary() {
        return grossSalary;
    }

    public void setGrossSalary(String grossSalary) {
        this.grossSalary = grossSalary;
    }

    public String getNetPay() {
        return netPay;
    }

    public void setNetPay(String netPay) {
        this.netPay = netPay;
    }

    public String getExpenditure() {
        return expenditure;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public void setExpenditure(String expenditure) {
        this.expenditure = expenditure;
    }
}