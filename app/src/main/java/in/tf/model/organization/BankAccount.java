package in.tf.model.organization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankAccount {

    @SerializedName("BankID")
    @Expose
    public Integer bankID;
    @SerializedName("BankName")
    @Expose
    public String bankName;
    @SerializedName("AccountNumber")
    @Expose
    public Long accountNumber;
    @SerializedName("IFSC")
    @Expose
    public String iFSC;

    public Integer getBankID() {
        return bankID;
    }

    public void setBankID(Integer bankID) {
        this.bankID = bankID;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getiFSC() {
        return iFSC;
    }

    public void setiFSC(String iFSC) {
        this.iFSC = iFSC;
    }
}