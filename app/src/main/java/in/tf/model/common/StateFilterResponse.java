package in.tf.model.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateFilterResponse {

    @SerializedName("key")
    @Expose
    public String key;
    @SerializedName("value")
    @Expose
    public String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}