package in.tf.model.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StateApiResponse {
    @SerializedName("list")
    @Expose
    public List<StateFilterResponse> list = new ArrayList<>();

    public List<StateFilterResponse> getList() {
        return list;
    }

    public void setList(List<StateFilterResponse> list) {
        this.list = list;
    }
}