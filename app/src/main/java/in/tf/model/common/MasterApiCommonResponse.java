package in.tf.model.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MasterApiCommonResponse {
    @SerializedName("list")
    @Expose
    public java.util.List<FilterResponse> list = new ArrayList<>();

    public List<FilterResponse> getList() {
        return list;
    }

    public void setList(List<FilterResponse> list) {
        this.list = list;
    }
}