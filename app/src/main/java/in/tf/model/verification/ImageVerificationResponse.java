package in.tf.model.verification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageVerificationResponse {

    @SerializedName("fileGUID")
    @Expose
    public String fileGUID;
    @SerializedName("fileName")
    @Expose
    public String fileName;
    @SerializedName("webLink")
    @Expose
    public String webLink;

}