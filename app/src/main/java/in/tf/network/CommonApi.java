package in.tf.network;

import android.content.Context;

import java.util.List;

import in.tf.model.common.FilterRequest;
import in.tf.model.common.FilterResponse;
import in.tf.model.common.MasterApiCommonResponse;
import in.tf.model.common.StateApiResponse;
import in.tf.model.organization.BankDetailsResponse;
import in.tf.util.AppLog;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CommonApi {
    public static void getStateList(Context context, FilterRequest filter, CommonApiCallback callback) {
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getStates(filter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<StateApiResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<StateApiResponse> objectResponse) {
                        StateApiResponse s = objectResponse.body();
                        callback.response(s);
                    }
                });
    }

    public static void getBankDetails(Context context, FilterRequest filter, CommonApiCallback callback) {
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getBanksList(filter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<BankDetailsResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<BankDetailsResponse> objectResponse) {
                        BankDetailsResponse s = objectResponse.body();
                        callback.response(s);
                    }
                });
    }

    public static void getCityList(Context context, FilterRequest filter, CommonApiCallback callback) {

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getCities(filter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<MasterApiCommonResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<MasterApiCommonResponse> objectResponse) {
                        MasterApiCommonResponse s = objectResponse.body();
                        callback.response(s);

                    }
                });
    }

    public static void getInstituteList(Context context, FilterRequest filter, CommonApiCallback callback) {

        //filter.setFilter("IN-UT");
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getInstitute(filter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<MasterApiCommonResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<MasterApiCommonResponse> objectResponse) {
                        String data = objectResponse.body().toString();
                        callback.response(data);

                    }
                });
    }

    public static void getOrganizationList(Context context, FilterRequest filter, CommonApiCallback callback) {

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getOrganization(filter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<MasterApiCommonResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<MasterApiCommonResponse> objectResponse) {
                        MasterApiCommonResponse data = objectResponse.body();
                        callback.response(data);
                    }
                });
    }

    public static void getDocumentTypes(Context context, FilterRequest filter, CommonApiCallback callback) {

        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.getDocumentsType(filter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<FilterResponse>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<List<FilterResponse>> objectResponse) {
                        List<FilterResponse> data = objectResponse.body();
                        callback.response(data);
                    }
                });
    }
}
