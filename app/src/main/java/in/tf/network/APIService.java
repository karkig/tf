package in.tf.network;

import java.util.List;
import java.util.Map;

import in.tf.model.common.StateApiResponse;
import in.tf.model.organization.BankDetailsResponse;
import in.tf.model.organization.GetIncomeDetailsRequest;
import in.tf.model.organization.OrganisationDetailRequestResponse;
import in.tf.model.personaldetails.PersonDetailRequest;
import in.tf.model.personaldetails.address.UserAddressRequest;
import in.tf.model.common.FilterRequest;
import in.tf.model.common.FilterResponse;
import in.tf.model.common.MasterApiCommonResponse;
import in.tf.model.personaldetails.PersonalDetailsDao;
import in.tf.model.registerlogin.Person;
import in.tf.model.registerlogin.PhoneRequest;
import in.tf.model.registerlogin.RegisterRequest;
import in.tf.model.registerlogin.RegisterStep1Response;
import in.tf.model.registerlogin.OTPMPinRequest;
import in.tf.model.registerlogin.ResetMPinRequest;
import in.tf.model.verification.ImageVerificationResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

/**
 * Created by Piyush on 14-01-2016.
 */
public interface APIService {

    String REGISTER_NUMBER = "user/registerphone/";
    String VERIFY_OTP = "user/confirmphone/";
    String REGISTER_USER = "user/completeregistration/";
    String LOGIN_API = "user/login/";
    String UPLOAD_IMAGE = "file/uploadimage";
    String UPLOAD_VIDEO = "file/uploadvideo";
    String PERSONAL_DETAILS = "profile/updatepersonaldetail";
    String STATES = "masterdata/states";
    String CITY = "masterdata/cities";
    String INSTITUTE = "masterdata/institutes";
    String ORGANIZATION = "masterdata/organisations";
    String DOCUMENTS_TYPE = "file/DocumentTypes";
    String UPDATE_ADDRESS = "profile/updatepersonsaddresses";
    String GET_PERSON_DETAILS = "profile/getperson";
    String UPDATE_INCOME_ADDRESS = "profile/updatepersonsincome";
    String GET_INCOME_DETAILS = "profile/getpersonsincome";
    String UPLOAD_PROFILE_IMAGE = "profile/UploadProfileImage";
    String BANK_LIST = "masterdata/banks";
    String OTP_REQUEST_FOR_RESET_PIN = "user/sendotp";
    String RESET_PIN = "user/ResetPIN";


    @FormUrlEncoded
    @retrofit2.http.POST(VERIFY_OTP)
    Observable<Response<RegisterStep1Response>> verifyOTP(@FieldMap Map<String, String> map);

    @retrofit2.http.POST(REGISTER_USER)
    Observable<Response<RegisterStep1Response>> registerUser(@Body RegisterRequest data);

    @FormUrlEncoded
    @retrofit2.http.POST(LOGIN_API)
    Observable<Response<RegisterStep1Response>> login(@FieldMap Map<String, String> map);

    @retrofit2.http.POST(REGISTER_NUMBER)
    Observable<Response<RegisterStep1Response>> registerPhoneNumber(@Body PhoneRequest map);

    @retrofit2.http.POST(PERSONAL_DETAILS)
    Observable<Response<Person>> updatePersonalDetails(@Body PersonalDetailsDao data);

    @retrofit2.http.POST(STATES)
    Observable<Response<StateApiResponse>> getStates(@Body FilterRequest data);

    @retrofit2.http.POST(CITY)
    Observable<Response<MasterApiCommonResponse>> getCities(@Body FilterRequest data);

    @retrofit2.http.POST(INSTITUTE)
    Observable<Response<MasterApiCommonResponse>> getInstitute(@Body FilterRequest data);

    @retrofit2.http.POST(ORGANIZATION)
    Observable<Response<MasterApiCommonResponse>> getOrganization(@Body FilterRequest data);

    @retrofit2.http.POST(DOCUMENTS_TYPE)
    Observable<Response<List<FilterResponse>>> getDocumentsType(@Body FilterRequest data);

    @retrofit2.http.POST(UPDATE_ADDRESS)
    Observable<Response<Person>> updateAddress(@Body UserAddressRequest data);

    @retrofit2.http.POST(UPDATE_INCOME_ADDRESS)
    Observable<Response<OrganisationDetailRequestResponse>> updateIncomeDetails(@Body OrganisationDetailRequestResponse data);

    @retrofit2.http.POST(GET_PERSON_DETAILS)
    Observable<Response<Person>> getPersonDetails(@Body PersonDetailRequest data);

    @retrofit2.http.POST(GET_INCOME_DETAILS)
    Observable<Response<OrganisationDetailRequestResponse>> getIncomeDetails(@Body GetIncomeDetailsRequest data);

    @retrofit2.http.POST(BANK_LIST)
    Observable<Response<BankDetailsResponse>> getBanksList(@Body FilterRequest data);

    @Multipart
    @POST(UPLOAD_IMAGE)
    Observable<ImageVerificationResponse> saveImage(@Part("userid") RequestBody id,
                                                    @Part("documenttype") RequestBody fullName,
                                                    @Part MultipartBody.Part image
    );

    @Multipart
    @POST(UPLOAD_PROFILE_IMAGE)
    Observable<ImageVerificationResponse> saveProfileImage(@Part("userid") RequestBody id,
                                                           @Part("documenttype") RequestBody fullName,
                                                           @Part MultipartBody.Part image
    );

    @Multipart
    @POST(UPLOAD_VIDEO)
    Observable<ImageVerificationResponse> saveVideo(@Part("userid") RequestBody id,
                                                    @Part("documenttype") RequestBody fullName,
                                                    @Part MultipartBody.Part video
    );
    /*  @Headers({
            "app_id:" + OxfordConfig.Application_ID + "",
            "app_key:" + OxfordConfig.Application_Key + "",
            "Accept: application/json"
    })
    @retrofit2.http.GET
    Observable<Response<WordInformationResponse>> getOxfordWordDetail(@Url String url);

*/
    @retrofit2.http.POST(OTP_REQUEST_FOR_RESET_PIN)
    Observable<Response<String>> requestForOtp(@Body OTPMPinRequest data);

    @retrofit2.http.POST(RESET_PIN)
    Observable<Response<String>> resetMpin(@Body ResetMPinRequest data);

}