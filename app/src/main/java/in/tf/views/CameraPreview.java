package in.tf.views;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by home on 1/11/16.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    SurfaceHolder mHolder;
    Camera videoCamera;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        videoCamera = camera;
        mHolder =getHolder();
        mHolder.addCallback(this);
        //deprecated but needed prior android 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void refreshVideoCamera(Camera camera){
        if(mHolder.getSurface()==null) {
            //preview surface does not exist
            return;
        }
        //stop preview before making any changes
        try {
            if(videoCamera != null){
            videoCamera.stopPreview();
            }
        }catch (Exception e){
            Log.e("camera Preview"," failed to stop preview " +e.getMessage());
        }
        //make any changes to camera like rotation , size etc here
        //start preview with new settings
        setVideoCamera(camera);
        try {
         videoCamera.setPreviewDisplay(mHolder);
         videoCamera.startPreview();
        }catch (Exception e){
            Log.e("Video Camera", "Got exception in preview "+e.getMessage()+"");
        }

    }

    public void setVideoCamera(Camera camera){
        videoCamera=camera;
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
      //create surface and start camera preview .
        try{
        if(videoCamera==null){
            videoCamera.setPreviewDisplay(holder);
            videoCamera.startPreview();
        }
        }catch (Exception e){
            Log.e("Video Camera", "Got exception in preview "+e.getMessage()+"");
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    //stop preview before making any changes to camera
    //refresh camera when resuming activity
      refreshVideoCamera(videoCamera);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
