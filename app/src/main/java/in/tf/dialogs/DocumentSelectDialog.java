package in.tf.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.tf.R;
import in.tf.listeners.OnDocumentTypeSelected;

public class DocumentSelectDialog extends Dialog {

    @BindView(R.id.all)
    RadioButton all;
    @BindView(R.id.identityProof)
    RadioButton identityProof;
    @BindView(R.id.addressProof)
    RadioButton addressProof;
    @BindView(R.id.earningProof)
    RadioButton earningProof;

    public Activity context;
    public Dialog d;
    OnDocumentTypeSelected callback;

    public DocumentSelectDialog(Activity cnt, OnDocumentTypeSelected callback) {
        super(cnt);
        this.callback = callback;
        this.context = cnt;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_document_select);
        ButterKnife.bind(this);
        all.setOnClickListener(view -> {
            callback.onDocSelected((String) view.getTag());
            dismiss();
        });
        identityProof.setOnClickListener(view -> {
            callback.onDocSelected((String) view.getTag());
            dismiss();
        });
        addressProof.setOnClickListener(view -> {
            callback.onDocSelected((String) view.getTag());
            dismiss();
        });
        earningProof.setOnClickListener(view -> {
            callback.onDocSelected((String) view.getTag());
            dismiss();
        });
    }

}