package in.tf.listeners;

public interface OnDocumentTypeSelected {
    void onDocSelected(String tag);
}
