package in.tf.listeners;

public interface OTPListener {
    void onOTPReceived(String otp);

}
