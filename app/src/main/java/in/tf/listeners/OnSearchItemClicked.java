package in.tf.listeners;

/**
 * Created by kailash on 25/2/18.
 */

public interface OnSearchItemClicked {
    void onItemClicked(String text);
}
