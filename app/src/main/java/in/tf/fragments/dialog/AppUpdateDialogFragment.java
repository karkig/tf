package in.tf.fragments.dialog;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import in.tf.BuildConfig;
import in.tf.R;

/**
 * Created by kailash on 3/22/2016.
 */
public class AppUpdateDialogFragment extends DialogFragment {

    private TextView update;
    private boolean isAForceUpdate = false;
    private TextView dialogMsz;
    private TextView currentVer;
    TextView skip;

    public AppUpdateDialogFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView =null;
        return rootView;
    }



    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener((dialog, keyCode, event) -> {
            if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                return true; // pretend we've processed it
            } else
                return true; // pass on to be processed as normal
        });
    }

}
